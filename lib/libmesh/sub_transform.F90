SUBROUTINE  sub_transform(a,point,point1)
  USE PRECISION
  USE MESH
  USE DATA
  IMPLICIT NONE
  REAL(dp) :: point(dim),a(Nverticesperelem,dim),det,point1(dim)&
       &,mat(dim,dim)
  INTEGER :: INFO,I,J,IPIV(dim)

  DO I=1,dim
     DO J=1,dim
        mat(I,J)=a(J+1,I)-a(1,I)
     END DO
  END DO

  point1=point-a(1,:)
IF(dp.EQ.8) THEN
  CALL DGETRF(dim,dim,mat,dim,IPIV,INFO)
  CALL DGETRS('N',dim,1,mat,dim,IPIV,point1,dim,INFO)
ELSE
   CALL SGETRF(dim,dim,mat,dim,IPIV,INFO)
   CALL SGETRS('N',dim,1,mat,dim,IPIV,point1,dim,INFO)
END IF
END SUBROUTINE sub_transform




