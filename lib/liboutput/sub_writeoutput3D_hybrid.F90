SUBROUTINE sub_writeoutput3D_helmholtz_hybrid
  USE PRECISION
  USE DATA
  USE mesh
  USE solution
  USE MUMPS
  USE analytic
  USE MPI_MODIF

  IMPLICIT NONE 
  INTEGER :: I,J,idx,deg,size_vect,nb_rcv_glob,num1,num2,num3
  CHARACTER*100 :: nam1,nam2,nam3
  CHARACTER*100 :: nam_sisvx,nam_sisvy,nam_sisvz
  real(kind=dp) :: t1,t2
  INTEGER, allocatable :: rcv_glob(:)
  
  Nbinst=0

  IF(nb_rcv.NE.0) THEN
     WRITE(nam2,*) myrank
     IF (myrank.LE.9) THEN
        nam2="000"//TRIM(ADJUSTL(nam2))
     ELSEIF (myrank.LE.99) THEN 
        nam2="00"//TRIM(ADJUSTL(nam2))
     ELSEIF (myrank.LE.999) THEN
        nam2="0"//TRIM(ADJUSTL(nam2))
     END IF
     nam_sisvx="FILM/sismo_Vx_"//TRIM(ADJUSTL(nam2))//".dat"
     nam_sisvy="FILM/sismo_Vy_"//TRIM(ADJUSTL(nam2))//".dat"
     nam_sisvz="FILM/sismo_Vz_"//TRIM(ADJUSTL(nam2))//".dat"
     num1=24+myrank
     num2=25+myrank
     num3=26+myrank
     OPEN(num1,FILE=TRIM(ADJUSTL(nam_sisvx)))
     OPEN(num2,FILE=TRIM(ADJUSTL(nam_sisvy)))
     OPEN(num3,FILE=TRIM(ADJUSTL(nam_sisvz)))
!!$     call mpi_barrier(mpi_in_place,ierr)
!!$     allocate(rcv_glob(nb_rcv_glob))
!!$     rcv_glob=0
!!$     if (nb_rcv.ne.0) then
!!$        DO i=1,nb_rcv
!!$           Rcv_glob(Idx_rcv_glob(i))=I_rcv_Glob(1,i)
!!$        enddo
!!$     endif
!!$
!!$     CALL MPI_ALLREDUCE(MPI_IN_PLACE,rcv_glob,nb_rcv_glob, MPI_INTEGER, mpi_sum,id%comm,ierr)
!!$     if (myrank.eq.0) then
!!$        OPEN(24,FILE="FILM/sismo_Vx.dat")
!!$        OPEN(25,FILE="FILM/sismo_Vy.dat")
!!$        OPEN(26,FILE="FILM/sismo_Vz.dat")
!!$     endif
  END IF
  
  if (solver.eq.1) then
     CALL MPI_Bcast(id%rhs,index_edge(Nfaces)*id%nrhs, MPI_DOUBLE_COMPLEX, 0,id%comm,ierr)
  elseif (solver.eq.2 .or. solver.eq.3) then
#ifdef WITH_MAPHYS
     call allreduce_maphys_sol
#endif
  endif
  
  if (nflu.NE.0 .and. nsol.eq.0) then
     size_vect=SUM(index_tri(Triloc2glob(1:Nflusol_loc+Nflu_loc)-1))
     ALLOCATE(Vxa_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     ALLOCATE(Vya_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     ALLOCATE(Vza_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     ALLOCATE(P_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     
     call cpu_time(t1)

     CALL solve_element_problem3D_acoustic !!lib/libmat/sub_defstiffmat_hybrid
     
     call cpu_time(t2)
     write(6,*) 'Resolution for each element time :', t2-t1

     print*,maxval(real(Vxa_cmplx)),'vx max a'
     print*,minval(real(Vxa_cmplx)),'vx min a'

     DO I=1,nb_src
        nam1="Vacous"
        nam2=""
        CALL sub_writeunstruct3D_vacous_binary(nam1,I)
        nam2="P"
        nam3=""
        CALL sub_writeunstruct3D_p_binary(nam2,nam3,I)
     ENDDO
     nam1="Paramflu"
     CALL sub_writeunstruct3D_paramflu_binary(nam1,nam3,-1)
  elseif (nsol .ne. 0 .and. nflu.eq.0) then
     size_vect=SUM(index_tri(Triloc2glob(Nflusol_loc+Nflu_loc&
          &+1:NTri_loc))&
          &-index_tri(Triloc2glob(Nflusol_loc+Nflu_loc&
          &+1:NTri_loc)-1))
     ALLOCATE(Vx_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     ALLOCATE(Vy_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     ALLOCATE(Vz_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     ALLOCATE(Sxx_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     ALLOCATE(Syy_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     ALLOCATE(Szz_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     ALLOCATE(Sxy_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     ALLOCATE(Sxz_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     ALLOCATE(Syz_cmplx(Ntri_loc*Nphi(degree_max)*id%nrhs))
     
     call cpu_time(t1)
     
     CALL solve_element_problem3D !!lib/libmat/sub_defstiffmat_hybrid
     
     call cpu_time(t2)
     write(6,*) 'Resolution for each element time :', t2-t1
     
     IF ((analytic_global%type_analytic .EQ. 101) .OR. (analytic_global%type_analytic .EQ. 201)) THEN
        print*,'diffraction output'
        !! pour le cas test de diffraction par un cercle, on enlève l'onde incidente de la solution finale (calcul en champ total, on veut l'onde diffractee)
        CALL sub_incident_wave3D
     ENDIF
     
     print*,maxval(real(Vx_cmplx)),'vx max a'
     print*,minval(real(Vx_cmplx)),'vx min a'
     
     DO I=1,nb_src
        nam1="V"
        CALL sub_writeunstruct3D_v_binary(nam1,I)
        nam2="S"
        CALL sub_writeunstruct3D_s_binary(nam2,I)
     ENDDO
     nam1="Paramsol"
     CALL sub_writeunstruct3D_paramsol_binary(nam1,nam3,-1)
!!$  CALL sub_writeunstruct_v(nam1,-1)
  endif

  IF(nb_rcv.NE.0) THEN
     CALL sub_write_sismos_hybrid(nb_src,Ntri_loc*Nphi(degree_max)*id%nrhs, Vx_cmplx, 1, num1)
     CALL sub_write_sismos_hybrid(nb_src,Ntri_loc*Nphi(degree_max)*id%nrhs, Vy_cmplx, 2, num2)
     CALL sub_write_sismos_hybrid(nb_src,Ntri_loc*Nphi(degree_max)*id%nrhs, Vz_cmplx, 3, num3)
  END IF
  
  IF(nb_rcv.NE.0) THEN
     CLOSE(num1)
     CLOSE(num2)
     CLOSE(num3)
  END IF
  
END SUBROUTINE sub_writeoutput3D_helmholtz_hybrid


SUBROUTINE sub_incident_wave3D
  USE PRECISION
  USE DATA
  USE MESH
  USE MUMPS
  USE SOLUTION
  USE analytic
  
  INTEGER :: I,J,deg,Node(3),int_j,indice,KK,Level,II,JJ
  COMPLEX(KIND=dp) :: icpl,Vx_inc(Nphi(degree_max)*Ntri_loc),Vz_inc(Nphi(degree_max)*Ntri_loc) &
       &, Sxx_inc(Nphi(degree_max)*Ntri_loc),Szz_inc(Nphi(degree_max)*Ntri_loc),Sxz_inc(Nphi(degree_max)*Ntri_loc)
  REAL(KIND=dp) :: kx,ky,angle,vp,vs,x(Nphi(degree_max)),y(Nphi(degree_max)) &
       &, V12(2),V23(2),V31(2),freq, ampinc

  icpl=dcmplx(0.0, 1.0)
  ampinc=0.1

  Vx_inc = 0.0
  Vz_inc = 0.0
  Sxx_inc = 0.0 
  Szz_inc = 0.0 
  Sxz_inc = 0.0 
  JJ=0
  DO II=1,Ntri_loc
     I=Triloc2glob(II)
     deg=degree_tri(I)
     Node=Tri(I,:)
   
     DO int_j=1,3
        x(int_j)=coor(node(int_j),1)
        y(int_j)=coor(node(int_j),2)
     END DO
     IF (deg.GE.2) THEN
        V12(1)=Coor(Node(2),1)-Coor(Node(1),1)
        V12(2)=Coor(Node(2),2)-Coor(Node(1),2)
        V23(1)=Coor(Node(3),1)-Coor(Node(2),1)
        V23(2)=Coor(Node(3),2)-Coor(Node(2),2)
        V31(1)=Coor(Node(1),1)-Coor(Node(3),1)
        V31(2)=Coor(Node(1),2)-Coor(Node(3),2)
        indice=3
        DO KK=1,deg-1
           indice=indice+1
           x(indice)=Coor(node(1),1)+KK*V12(1)/deg
           y(indice)=Coor(node(1),2)+KK*V12(2)/deg            
        ENDDO
        DO KK=1,deg-1
           indice=indice+1
           x(indice)=Coor(Node(2),1)+KK*V23(1)/deg
           y(indice)=Coor(Node(2),2)+KK*V23(2)/deg
        ENDDO
        DO KK=1,deg-1
           indice=indice+1
           x(indice)=Coor(Node(3),1)+KK*V31(1)/deg
           y(indice)=Coor(Node(3),2)+KK*V31(2)/deg
        ENDDO
     END IF
     IF (deg.GE.3) THEN
!!! We run over all the subtriangles
        DO Level=1,(degree_tri(i)-3)/3+1
           indice=indice+1
           x(indice)=Coor(Node(1),1)+Level*V12(1)/REAL(deg,dp)-Level*V31(1)/REAL(deg,dp)
           y(indice)=Coor(Node(1),2)+Level*V12(2)/REAL(deg,dp)-Level*V31(2)/REAL(deg,dp)
           !We run over the third edge of the triangle level 
           DO KK=1,deg-3*Level
              indice=indice+1
              x(indice)=x(indice-1)+V12(1)/REAL(deg,dp)
              y(indice)=y(indice-1)+V12(2)/REAL(deg,dp)
           END DO
           !We run over the second edge of the triangle level 
           DO KK=1,deg-3*Level
              indice=indice+1
              x(indice)=x(indice-1)+V23(1)/REAL(deg,dp)
              y(indice)=y(indice-1)+V23(2)/REAL(deg,dp)
           END DO
           !We run over the first edge of the triangle level 
           DO KK=1,deg-3*Level-1
              indice=indice+1
              x(indice)=x(indice-1)+V31(1)/REAL(deg,dp)
              y(indice)=y(indice-1)+V31(2)/REAL(deg,dp)
           END DO
        END DO
     END IF

     IF(ABS(Cij(I,1,3)+Cij(I,2,3)).GT.1e-8) THEN
        angle= ACOS((Cij(I,1,1)-Cij(I,2,2))/2. &
             &/SQRT((Cij(I,1,3)+Cij(I,2,3))**2 +(Cij(I &
             &,1,1)-Cij(I,2,2))**2/4.))/2.
     ELSE
        angle=0.
     END IF
     vp=SQRT((SIN(angle)**4*Cij(I,1,1)+COS(angle)**4*Cij(I &
          &,2,2)+2.*COS(angle)**2 *SIN(angle)**2*(2.*Cij(I,3,3)&
          & +Cij(I,1,2))-4.*SIN(angle)**3 *COS(angle)*Cij(I&
          & ,1,3)-4.*SIN(angle) *COS(angle)**3*Cij(I,2,3)) &
          &/rho(I))
     vs=SQRT((COS(angle)**2*SIN(angle)**2*(Cij(I,1,1)+Cij(I&
          & ,2,2))+(COS(angle)**2 -SIN(angle)**2)**2*Cij(I,3,3)&
          & -2.*COS(angle)* SIN(angle)*(COS(angle)**2-SIN(angle)&
          &**2) *(Cij(I,1,3)-Cij(I,2,3))-2.*COS(angle)**2 &
          &*SIN(angle)**2*Cij(I,1,2))/rho(I)) 
     
     kx = omega*cos(theta0)/vp
     ky = omega*sin(theta0)/vp

!! pour le solide élastique, on enlève U_inc que dans la couronne i.e. milieu 2
     IF ((analytic_global%type_analytic .EQ. 201 .AND. Ref_Media_loc(II).EQ.2)&
          & .OR.(analytic_global%type_analytic .EQ. 101))  THEN
        DO J=1,Nphi(deg)
           IF (theta0 .NE. pi/2.0) THEN
              Vx_inc(JJ+J) = ampinc*exp(icpl*(-x(J)*kx-y(J)*ky))
              Vz_inc(JJ+J) = kx*ky*(Cij(I,1,2) + Cij(I,3,3))* &
                   & 1.0/(rho(I)*(omega**2 - (kx*vs)**2 - (ky*vp)**2))*Vx_inc(JJ+J)
              Sxx_inc(JJ+J) = -1.0/omega*(Cij(I,1,1)*kx*Vx_inc(JJ+J) + &
                   & Cij(I,1,2)*ky*Vz_inc(JJ+J))
              Szz_inc(JJ+J) = -1.0/omega*(Cij(I,1,1)*ky*Vz_inc(JJ+J) + &
                   & Cij(I,1,2)*kx*Vx_inc(JJ+J))
              Sxz_inc(JJ+J) = -(Cij(I,3,3)/omega)*(kx*Vz_inc(JJ+J) + &
                   & ky*Vx_inc(JJ+J))
           ELSE
              Vx_inc(JJ+J) = ampinc*exp(icpl*(-x(J)*kx-y(J)*ky))
              Vz_inc(JJ+J) = 0.0
              Sxx_inc(JJ+J) = 0.0
              Szz_inc(JJ+J) = 0.0
              Sxz_inc(JJ+J) = -(Cij(I,3,3)/vs)*ky*ampinc*exp(icpl*(-x(J)*kx-y(J)*ky))
           ENDIF
        ENDDO
        JJ=JJ+Nphi(deg)
     ELSEIF (analytic_global%type_analytic .EQ. 201 .AND. Ref_Media_loc(II).EQ.1) THEN !! pour le milieu 1 (solide élastique), on met qd meme U_inc à zero pr avoir les bonnes correspondances avec les ddl!
        DO J=1,Nphi(deg)
           Vx_inc(JJ+J) = 0.0
           Vz_inc(JJ+J) = 0.0
           Sxx_inc(JJ+J) = 0.0
           Szz_inc(JJ+J) = 0.0
           Sxz_inc(JJ+J) = 0.0
        ENDDO
        JJ=JJ+Nphi(deg)
     ENDIF
  ENDDO

  freq=omega/(2.0*2._dp*ASIN(1._dp))
  Vx_cmplx = -(2**(freq-2))*(vp/10**4)*(Vx_cmplx - Vx_inc)!(vp/10**5)*(Vx_cmplx - Vx_inc)!freq/2*(vp(1)/1e4)
  Vz_cmplx =  -(2**(freq-2))*(vp/10**4)*(Vz_cmplx - Vz_inc)! (vp/10**5)*(Vz_cmplx - Vz_inc)
  Sxx_cmplx = -(2**(freq-2))*(vp/10**4)*(Sxx_cmplx - Sxx_inc)!(vp/10**5)*(Sxx_cmplx - Sxx_inc)
  Szz_cmplx = -(2**(freq-2))*(vp/10**4)*(Szz_cmplx - Szz_inc)!(vp/10**5)*(Szz_cmplx - Szz_inc)
  Sxz_cmplx = -(2**(freq-2))*(vp/10**4)*(Sxz_cmplx - Sxz_inc)!(vp/10**5)*(Sxz_cmplx - Sxz_inc)
!!$  Vx_cmplx = (-freq/2.0)*(vp/10**5)*(Vx_cmplx - Vx_inc)!(vp/10**5)*(Vx_cmplx - Vx_inc)!freq/2*(vp(1)/1e4)
!!$  Vz_cmplx =  (-freq/2.0)*(vp/10**5)*(Vz_cmplx - Vz_inc)! (vp/10**5)*(Vz_cmplx - Vz_inc)
!!$  Sxx_cmplx = (freq/2.0)*(vp/10**5)*(Sxx_cmplx - Sxx_inc)!(vp/10**5)*(Sxx_cmplx - Sxx_inc)
!!$  Szz_cmplx = (freq/2.0)*(vp/10**5)*(Szz_cmplx - Szz_inc)!(vp/10**5)*(Szz_cmplx - Szz_inc)
!!$  Sxz_cmplx = (freq/2.0)*(vp/10**5)*(Sxz_cmplx - Sxz_inc)!(vp/10**5)*(Sxz_cmplx - Sxz_inc)

  if (analytic_global%type_analytic .EQ. 201)  THEN
     Vx_cmplx = -Vx_cmplx
     Vz_cmplx = -Vz_cmplx
!!$     Sxx_cmplx= -Sxx_cmplx
!!$     Szz_cmplx= -Szz_cmplx
!!$     Sxz_cmplx= -Sxz_cmplx
  endif
  if (analytic_global%type_analytic .EQ. 101 .AND. freq.EQ.4.0)  THEN
!!$     Vx_cmplx = -Vx_cmplx
!!$     Vz_cmplx = -Vz_cmplx
     Sxx_cmplx= -Sxx_cmplx
     Szz_cmplx= -Szz_cmplx
     Sxz_cmplx= -Sxz_cmplx
  endif

END SUBROUTINE sub_incident_wave3D

#ifdef WITH_MAPHYS
SUBROUTINE allreduce_maphys_sol
  use maphys
  use precision
  USE DATA
  USE mesh
  USE solution
  use mpi_modif
  use mumps

  INTEGER :: III

  ALLOCATE(rhs_maphys(nphi_face_global))
  rhs_maphys=0.0

  DO III=1,myndof
     rhs_maphys(elemloc2glob(III))=mphs%sol(III)
  ENDDO

  DO III=myndof-mysizeintrf+1,myndof
     rhs_maphys(elemloc2glob(III))=rhs_maphys(elemloc2glob(III))/2 !! division par 2, car valeur sur chacun des procs
  ENDDO

  call MPI_ALLREDUCE(MPI_IN_PLACE,rhs_maphys,nphi_face_global,MPI_DOUBLE_COMPLEX, mpi_sum,mpi_comm_world,ierr)
  ALLOCATE(id%rhs(nphi_face_global))
  id%rhs=rhs_maphys
ENDSUBROUTINE allreduce_maphys_sol
#endif
