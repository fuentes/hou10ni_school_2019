!> @file 
!! This file contains the main program
!> @brief 
!! @author Julien Diaz
!! @version 2.0
!! @todo Implementation of High Order ABC; 
!! @todo Definitio



!! @todo Place sismos and  snapshot in an appropriate subroutine; 


!> @routine
!! This is the main program
PROGRAM hou10ni_helmholtz
  USE PRECISION
  USE CONSTANT
  USE DATA
  USE mesh
  USE solution
  USE analytic 
  USE PML
  USE mpi_modif
  USE mumps
#ifdef WITH_MAPHYS
  USE maphys
#endif
  USE transient

  IMPLICIT NONE 



  INTEGER :: irhs
  REAL(kind=dp) :: t1,t2,coor_x,coor_y,maxx,minx,maxy,miny,maxz,minz,dt1 &
       &,energ,energ_tot,dt_source,&
       xmin,xmax,ymin,ymax,zmin,zmax,xlen,ylen,zlen,h_min_ad
  REAL(kind=8) :: tol
  !> @param I Integer for DO LOOPS
  !> @param J Integer for DO LOOPS
  !> @param deg The degree of a given triangle
  !> @param idx Indicates the number of degrees of the freedom used until the
  ! given triangle
  INTEGER :: I,J,I_tmp,ineigh,deg,idx,omp_get_thread_limit&
       &,omp_get_num_threads,procI&
       &,K,L,II,JJ&
       &,iunit_maphys, dummy
!! Uncomment for computing the energy
!!  REAL(kind=dp),ALLOCATABLE :: A_sol_bis(:)

  CHARACTER*100 :: nam,nam1,nam2,nam3
  character(1024)             :: matrixfile,rhsfile,initguessfile
  character(1024)             :: outrhsfile,outsolfile

  IF(dp.EQ.4) THEN
     MPI_REAL_PERSO=MPI_REAL
     MPI_COMPLEX_PERSO=MPI_COMPLEX
  ELSE
     MPI_REAL_PERSO=MPI_DOUBLE_PRECISION
     MPI_COMPLEX_PERSO=MPI_DOUBLE_COMPLEX
  END IF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!! Initialization for MPI
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  CALL MPI_INIT(IERR)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,nproc,ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,myrank,ierr)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!! End for MPI
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  id%comm=MPI_COMM_WORLD
  WRITE(6,*) 'my rank is ',myrank
  WRITE(6,*) 'number of ranks ',nproc

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!! Reading Data
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  CALL sub_readdata  !! In lib/libinit
!!$  PRINT*,'hybrid', hybrid

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!! Initialization for MUMPS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  IF(symmetry.EQ.1) THEN
     id%SYM = 2
  ELSE
     id%sym=0
  END IF


  num_thread=MIN(1,omp_get_thread_limit())
  WRITE(6,*) 'num_thread',num_thread
  id%PAR = 1
  id%JOB = -1
  CALL ZMUMPS(id)
  myrank=id%myid
  

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!! End for MUMPS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!! Reading Mesh
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  IF (myrank  .EQ. 0 ) THEN
     CALL sub_readmesh  !! In lib/libmesh
     
     CALL sub_renum  !! In lib/libmesh

     IF(curved.EQ.1) THEN
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!! Reading the geometry of curved elements
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        IF (DIM.EQ.3) THEN
           WRITE(6,*) 'no curved elements in 3D'
           STOP 
        END IF
        IF (hybrid.EQ.1) THEN
           WRITE(6,*) 'no curved elements for hybrid'
           STOP 
        END IF
        CALL sub_curved  !!  In lib/libmesh
     END IF

     IF (hybrid.EQ.1) THEN
        CALL sub_hybrid
!!$        omega=2.0*pi*omega
!!$        CALL MPI_Bcast(omega,1, MPI_REAL_PERSO, 0,id%comm,ierr )
     ENDIF
  END IF
  if (hybrid.eq.1) then
     omega=2.0*pi*omega
     id%nrhs=nb_src
     if (type_src.eq.1) then
        id%nrhs=1
        nb_src=1
     endif
!!$     print*,'ntri',nflu,nsol,nflusol,nsolflu,nsol+nsolflu,nflu+nflusol
!!$     print*,'nedge',nedge_acous,nedge_acouselas,nedge_elas
!!$     print*,'nphi_edge',nphi_edge_acous,nphi_edge_acouselas_elas,nphi_edge_global,nphi_edge_sg
!!$     stop
  endif

  CALL sub_mesh_mpi !! In lib/libmesh/sub_readmesh.F90
  CALL sub_param_phys !!  In lib/libinit
  CALL sub_init

  if (nflusol.ne.0 .and. nsolflu.ne.0) then
     if (hybrid.eq.0) then
        condi=(omega)**2!omega**2
        condi2=1.0/10.0
     elseif (hybrid .eq. 1) then
        condi=1.0!(omega)**2!omega**2
        condi2=1.0!/1000.0
     endif
  else
     condi=1.0
     condi2=1.0
  endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      MAPHYS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if (solver.eq.2 .or. solver.eq.3) then
#ifdef WITH_MAPHYS
!!!!!!!!!!!!!!! Initialization for MAPHYS
     mphs%comm=MPI_COMM_WORLD
     mphs%JOB = -1
     CALL ZMPH_maphys_driver(mphs)
!!$     myrank=mphs%nodeid !!a verifier
     IF(symmetry.EQ.1) THEN
        mphs%SYM = 2
     ELSE
        mphs%sym=0
     END IF
     !! control parameters
     mphs%ICNTL(4)=5
     mphs%ICNTL(5)=1
     mphs%ICNTL(6)=2
     if (solver.eq.2) then
        mphs%ICNTL(13) = 1 !1: Mumps, 2:pastix
!!$     mphs%ICNTL(15) = 1 !1: Mumps, 2:pastix
     elseif (solver.eq.3) then
        mphs%ICNTL(13) = 2 !1: Mumps, 2:pastix
!!$     mphs%ICNTL(15) = 1 !1: Mumps, 2:pastix
     endif
     mphs%ICNTL(20) = 3
     
     !        Preconditionning technic
     mphs%ICNTL(21) = 5!10 !5:auto-detect the preconditionning technique
     mphs%RCNTL(11) = 1.0e-4
     
     mphs%ICNTL(22) = 3 !which orthogonalisation, default=3
     mphs%ICNTL(24) = 5000 !maximum number of iterations
     mphs%ICNTL(25) = 0
     mphs%ICNTL(26) = 5000
     
     mphs%ICNTL(27) = 0
     mphs%ICNTL(28) = 2
     mphs%ICNTL(30) = 0
!!$     mphs%ICNTL(32) = 1 !1: Mumps, 2:pastix , default=value of ICNTL(13)
     
     mphs%ICNTL(43) = 2 ! input system 1:centralized, 2:distributed
     mphs%RCNTL(21) = 1.0e-8
     !    specify how to bind thread inside MaPHyS
     mphs%ICNTL(36)=0
     !    control parameter that active the 2 levels of parallelism version
     mphs%ICNTL(42)=0
     !    useful, if mphs%ICNTL(42)/=0, default =0
!!$!           specify the number of nodes
!!$            mphs%ICNTL(37)
!!$!           specify the number of cores per node
!!$            mphs%ICNTL(38)
!!$!           specify the number of threads per domain
!!$            mphs%ICNTL(39)
!!$!           specify the number of domains
!!$            mphs%ICNTL(40)  
     mphs%ICNTL(45)=0
          matrixfile    = ''
     rhsfile       = ''
     initguessfile = ''
     outrhsfile    = ''
     outsolfile    = ''
     iunit_maphys = 42
     open(UNIT=iunit_maphys,FILE="conf_maphys.in",ACTION="read")
     call ZMPH_read_param_freeformat(                              &
               iunit_maphys,mphs % icntl,mphs % rcntl,mphs % sym,mphs % job, &
               matrixfile,rhsfile,initguessfile,                        &
               outrhsfile,outsolfile)          
     close(iunit_maphys)

     mphs%ICNTL(20) = 3
     IF(symmetry.EQ.1) THEN
        mphs%SYM=2
     ELSE
        mphs%SYM=0
     END IF

     call sub_construct_local_domains_maphys !! in lib/libmat/sub_construct_maphys

     CALL ZMPH_distributed_interface(mphs, myndof, mysizeIntrf, myinterface,&
             mynbvi, myindexvi, myptrindexvi, myindexintrf)
     !! a mettre dans le construct_local_domains_maphys
     mphs%N=myndof
     if (dim.eq.2) then
        mphs%NNZ=((2*3*nphi_face(degree_max))**2)*ntri_loc !!a modifier
     else
        mphs%NNZ=(((3*4*nphi_face(degree_max))**2)*ntri_loc+mphs%N) !!a modifier
     endif

     if (solver.eq.2) then
        mphs%ICNTL(13) = 1 !1: Mumps, 2:pastix
     elseif (solver.eq.3) then
        mphs%ICNTL(13) = 2 !1: Mumps, 2:pastix
     endif
     ! Set tolerance as a funtion of adimensionned h_max := max diameter of element
     call MPI_BCAST(h_min_adim,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)

     tol = (h_min_adim)!**(maxval(degree_tri+0.5))
     mphs%RCNTL(21) = tol

     
     ALLOCATE(mphs%RHS(mphs%N))
     ALLOCATE(mphs%SOL(mphs%N))
     mphs%RHS = 0.0
     mphs%SOL = 0.0
#endif   
  endif

  IF(curved.EQ.0) THEN
     IF (hybrid.EQ.0) THEN
        CALL sub_init_mat  !!  In lib/libmat/sub_defstiffmat
     ELSE
        print*,'init mat',myrank
        CALL sub_init_mat_hybrid !!! TODO :3D
     ENDIF
     Nbcla_flu=0
     Nbcla_sol=0
     IF (iscla==1) THEN
!!! On calcule les matrices associ�es aux CLA
        CALL sub_CLA !!! TODO :3D and time domain
     ENDIF

     CALL sub_init_mumps  !! In lib/libmat/sub_construct_mumps

#ifdef WITH_MAPHYS
     
     mphs%nnz = id%NZ_loc
     ALLOCATE(mphs%ROWS(mphs%NNZ))
     ALLOCATE(mphs%COLS(mphs%NNZ))
     ALLOCATE(mphs%VALUES(mphs%NNZ))
#endif
     CALL CPU_TIME(t1)
     IF (hybrid.EQ.0) THEN

        IF(Nflu+Nflusol.GT.0) THEN
           IF(dim.EQ.2) THEN
              CALL sub_defstiffmat_acoustic
           ELSE IF (dim.EQ.3) THEN
              CALL sub_defstiffmat3D_acoustic
           ENDIF
        END IF
        IF(Nsol+Nsolflu.GT.0) THEN
           IF(dim.EQ.2) THEN
              CALL sub_defstiffmat_elastic
           ELSE
              CALL sub_defstiffmat3D_elastic
           END IF
        END IF
        IF(Nflusol.GT.0) THEN
           IF(dim.EQ.2) THEN
              CALL sub_defstiffmat_elasto_acoustic
           ELSE IF (dim.EQ.3) THEN
              CALL sub_defstiffmat3D_elasto_acoustic
           ENDIF
        END IF

     ELSE 
        IF(Nflu+Nflusol.GT.0) THEN 
           IF(dim.EQ.2) THEN
              CALL sub_defstiffmat_acoustic_hybrid
           ELSE IF (dim.EQ.3) THEN 
              CALL sub_defstiffmat3D_acoustic_hybrid
           ENDIF
           print*,'acoustic ok'
        END IF
        IF(Nsol+Nsolflu.GT.0) THEN 
           IF (dim.EQ.2) THEN
              IF (compliance .EQ. 0) THEN
                 CALL sub_defstiffmat_elastic_hybrid
              ELSE
                 CALL sub_defstiffmat_elastic_hdgcomp
              ENDIF
              print*,'elastic ok'
           ELSE
              IF (compliance .EQ. 0) THEN
                 CALL sub_defstiffmat3D_elastic_hybrid
              ELSE
                 CALL sub_defstiffmat3D_elastic_hybrid_compliance
              ENDIF
              PRINT*,'defstiffmat3D_elastic_hybrid done'
           ENDIF
        ENDIF
        IF(Nflusol.GT.0 .AND. Nsolflu.GT.0) THEN
           IF(dim.EQ.2) THEN
              print*,'elastoacoustic'
              CALL sub_defstiffmat_elastoacoustic_hybrid
           ENDIF
        ENDIF
     ENDIF

!! curved elements
  ELSE
     CALL sub_init_mat_curved  !!  In lib/libmat/sub_defstiffmat


     IF (iscla==1) THEN
!!! On calcule les matrices associ�es aux CLA
        CALL sub_CLA_curved !! TODO : 3D and time
     ENDIF
!!$
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!Initialization of the mumps matrix
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

     CALL sub_init_mumps  !! In lib/libmat/sub_construct_mumps

#ifdef WITH_MAPHYS
     mphs%nnz = id%NZ_loc
     ALLOCATE(mphs%ROWS(mphs%NNZ))
     ALLOCATE(mphs%COLS(mphs%NNZ))
     ALLOCATE(mphs%VALUES(mphs%NNZ))
#endif  

     !The following subprogramms are in lib/libmat/sub_defstiffmat_courbe
     IF(Nflu_loc+Nflusol_loc.GT.0) THEN
        CALL sub_defstiffmat_acoustic_curved !! TODO : 3D and time and mpi
     END IF
     IF(Nsol_loc+Nsolflu_loc.GT.0) THEN
        CALL sub_defstiffmat_elastic_curved
     END IF
     IF(Nflusol_loc.GT.0) THEN
        CALL sub_defstiffmat_elasto_acoustic_curved !! TODO : 3D and time and mpi
     END IF
  END IF
  NBINST=0
!!! Attention, plein de chose � modifier ici
  IF(nb_rcv.NE.0) THEN
     CALL sub_rcv_localisation !! in lib/liboutput/sub_rcv_localisation.F90
  END IF

!!! Construction of the source
  IF (hybrid .EQ. 0) THEN
     IF(type_src.EQ.1) THEN
        ALLOCATE(id%rhs(index_tri(Ntri)))
        id%rhs=0.D0
        IF(curved.EQ.0) THEN
           IF (Nflusol.NE.0) THEN
              CALL sub_rhs_elastoacoustic
           ELSEIF (Nflu.NE.0) THEN
              IF(dim.EQ.2) THEN
                 CALL sub_rhs_neumann
                 !              CALL sub_rhs_acousticacoustic
              ELSE
                 CALL sub_rhs3D_acoustoneumann
              END IF
           ELSEIF (Nsol.NE.0) THEN
                 CALL sub_rhs_elastoelastic
              IF(dim.EQ.2) THEN
                 print*,'avant rhs'
!!$                 CALL sub_rhs_elastoneumann
              ELSE
                 CALL sub_rhs3D_elastoneumann
              END IF
           ENDIF
        ELSE
           IF (Nflusol.NE.0) THEN
              CALL sub_rhs_elastoacoustic_curved
           ELSEIF(Nflu.NE.0) THEN
              CALL sub_rhs_neumann_curved
           ELSEIF (Nsol.NE.0) THEN
              CALL sub_rhs_elastoelastic_curved
           ENDIF
        ENDIF
        id%rhs(1:index_tri(nflu+nflusol))=id%rhs(1:index_tri(nflu+nflusol)) &
             &/condi/condi2
     ELSE
        CALL sub_rhs_pointsource
        IF(helmholtz.EQ.1) THEN
           IF(myrank.EQ.0) THEN
              ALLOCATE(id%rhs(index_tri(Ntri)*id%nrhs))
              id%lrhs=index_tri(Ntri)
              WRITE(6,*) id%nrhs,id%nz_rhs
              WRITE(6,*) id%IRHS_ptr
           END IF
        END IF
     ENDIF
  ELSE
     IF (Nflu.NE.0 .and. nflusol.eq.0) THEN
        IF (dim.EQ.2) THEN
           if (solver.eq.1) then
              ALLOCATE(id%rhs(index_edge(NEdge)))
              id%lrhs=index_edge(NEdge)
              id%rhs=0.0
              
              CALL sub_rhs_hybrid_acoustic
!!$              CALL sub_rhs_hybrid_elastoacoustic
              CALL MPI_ALLREDUCE(MPI_IN_PLACE,id%rhs,index_edge(NEdge), MPI_DOUBLE_COMPLEX, mpi_sum,id%comm,ierr)
           elseif (solver.eq.2 .or. solver.eq.3) then
#ifdef WITH_MAPHYS
              call sub_rhs_hybrid_acoustic_maphys
#endif
           endif
        ELSE
           if (solver.eq.1) then
              ALLOCATE(id%rhs(index_edge(Nfaces)))
              id%lrhs=index_edge(Nfaces)
              id%rhs=0.0
           
              CALL sub_rhs3D_hybrid_acoustic
              
              CALL MPI_ALLREDUCE(MPI_IN_PLACE,id%rhs,index_edge(Nfaces), MPI_DOUBLE_COMPLEX, mpi_sum,id%comm,ierr)
           elseif (solver.eq.2 .or. solver.eq.3) then
#ifdef WITH_MAPHYS
              call sub_rhs3D_hybrid_acoustic_maphys
#endif  
           endif
        ENDIF
     ELSEIF (Nsol.NE.0 .and. nsolflu.eq.0) THEN
        IF (dim.EQ.2) THEN
           if (solver .eq. 1) then
              ALLOCATE(id%rhs(index_edge(NEdge)*id%nrhs))
              id%lrhs=index_edge(NEdge)
              id%rhs=0.0
              
              CALL sub_rhs_hybrid
              CALL MPI_ALLREDUCE(MPI_IN_PLACE,id%rhs,index_edge(NEdge)*id%nrhs,&
                   MPI_DOUBLE_COMPLEX, mpi_sum,id%comm,ierr)
           elseif (solver.eq.2 .or. solver.eq.3) then
#ifdef WITH_MAPHYS
              call sub_rhs_hybrid_maphys
#endif
           endif

        ELSE
           if (solver.eq.1) then
              ALLOCATE(id%rhs(index_edge(Nfaces)*id%nrhs))
              id%lrhs=index_edge(Nfaces)
              id%rhs=0.0

              CALL sub_rhs3D_hybrid
!!$           PRINT*,'rhs 3d done'
              CALL MPI_ALLREDUCE(MPI_IN_PLACE,id%rhs,index_edge(Nfaces)*id%nrhs,&
                   MPI_DOUBLE_COMPLEX, mpi_sum,id%comm,ierr)
           elseif (solver.eq.2 .or. solver.eq.3) then
#ifdef WITH_MAPHYS
              call sub_rhs3D_hybrid_maphys
#endif
           endif
        ENDIF
     ELSEIF (Nflusol.NE.0 .and. Nsolflu.ne.0) THEN
        print*,'rhs elasto acoustic'
        if (dim.eq.2) then
!!$           ALLOCATE(id%rhs(index_edge(NEdge)))
           ALLOCATE(id%rhs(nphi_edge_global*id%nrhs))
           id%lrhs=nphi_edge_global
           id%rhs=0.0
           CALL sub_rhs_hybrid_elastoacoustic
           !! a revoir avec mrhs
           id%rhs(1:nphi_edge_acous)=id%rhs(1:nphi_edge_acous)/condi2
           CALL MPI_ALLREDUCE(MPI_IN_PLACE,id%rhs,nphi_edge_global,&
                & MPI_DOUBLE_COMPLEX, mpi_sum,id%comm,ierr)
        endif
     ENDIF
  ENDIF

  CALL CPU_TIME(t2)
  WRITE(6,*) 'Global matrix assemblage and RHS time :', t2-t1
  ! id%write_problem='global_mat.txt'
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!Computation of the global matrix
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  WRITE(6,*) 'Global system size', id%N, id%NZ

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!Computation of the cla if type_cla>=4
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! TODO
!!$     IF (type_cla.GE.4) THEN
!!$        CALL sub_construct_mumps_cla  !! In lib/libmat/
!!$
!!$     END IF
  
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
  CALL CPU_TIME(t1)
  IF (helmholtz.EQ.1) THEN
!!$     id%ICNTL(19)=1
     if (solver.eq.1) then
        id%ICNTL(7)=4
        id%ICNTL(18)=3
        id%ICNTL(11)=1
        id%ICNTL(14)=200
!!$        id%ICNTL(33)=1 !! computes matrix determinant
        id%JOB = 6
        CALL  ZMUMPS(id)
        print *, "================================"
        print *, "DBG ", id%nrhs, id%nrhs
        print *, "================================"
        CALL CPU_TIME(t2)
        WRITE(6,*) 'Analysis, factorization and resolution time :', t2-t1
!!$        id%write_problem='mat_res.txt'

     elseif (solver .eq. 2 .or. solver.eq.3) then
#ifdef WITH_MAPHYS
!!$        !        Analysis phase
!!$        !        -------------------------------------------
!!$        mphs%JOB = 1
!!$        CALL ZMPH_maphys_driver(mphs)
!!$        
!!$        !        Factorization phase
!!$        !        -------------------------------------------
!!$        mphs%JOB = 2
!!$        CALL ZMPH_maphys_driver(mphs)
!!$
!!$        !        Preconditionning phase
!!$        !        -------------------------------------------
!!$        mphs%JOB = 3
!!$        CALL ZMPH_maphys_driver(mphs)
!!$       
!!$        !        Solution phase
!!$        !        -------------------------------------------
!!$        mphs%JOB = 4
!!$        CALL ZMPH_maphys_driver(mphs)
        mphs%JOB = 6
        CALL ZMPH_MAPHYS_DRIVER(mphs)
        CALL CPU_TIME(t2)
        WRITE(6,*) 'Analysis, factorization and resolution time :', t2-t1
!        call allreduce_maphys_sol
        id%nrhs = 1
        id%lrhs = 1
#endif
     endif


     IF (hybrid .EQ. 0) THEN
        !! we write the seismo and the snapshots
        IF(dim.EQ.2) THEN
           IF (type_rtm.EQ.1) THEN
              nam2='ref_5000000'
              nam3='ref_' 
           ELSEIF (type_rtm.EQ.2) THEN
              nam2='5000000' 
              nam3='' 
           ELSEIF (type_rtm.EQ.3) THEN
              nam2='RTM_5000000' 
              nam3='RTM_' 
           ELSE 
              nam2='' 
              nam3='' 
           END IF
           if (solver.eq.2 .or. solver.eq.3) then
           endif
           CALL sub_writeoutput_helmholtz(nam2,nam3) !!in lib/liboutput
        ELSE 
           CALL sub_writeoutput3D_helmholtz(nam2,nam3) !!in lib/liboutput
        END IF
        IF (analytic_global%type_analytic.GT.0) THEN
           !we compute the anayltic solution
           CALL sub_analytic
        END IF
     ELSE
        write(6,*) 'toto', dim
        IF(dim.EQ.2) THEN
           CALL sub_writeoutput_helmholtz_hybrid !!in lib/liboutput
        ELSE 
        write(6,*) 'toto2', dim
           CALL sub_writeoutput3D_helmholtz_hybrid !!in lib/liboutput
!!$           PRINT*, 'output hybrid 3D done'
        END IF

        WRITE(6,*) 'Work space: ', id%INFO(16), id%INFO(22),id%INFO(26)
        IF (analytic_global%type_analytic.GT.0) THEN
           !we compute the anayltic solution
           IF(dim.EQ.2) THEN
              CALL sub_analytic_hybrid
           ELSE
              CALL sub_analytic3D_hybrid
!!$              PRINT*,'analytic 3d done'
           ENDIF
        END IF

     END IF
     if (solver .eq. 1) then 
        id%job=-2
        CALL zmumps(id)
     elseif (solver .eq. 2 .or. solver.eq.3) then
#ifdef WITH_MAPHYS
        mphs%JOB=-2
        CALL ZMPH_MAPHYS_DRIVER(mphs)
#endif
     endif
  ELSE
     IF (dim.EQ.2) THEN
        IF(nflu.GT.0) THEN
           nam2="Paramflu"
           nam3=""
           CALL sub_writeunstruct_paramflu_binary(nam2,nam3,-1)
        END IF
        IF(nsol.GT.0) THEN
           nam2="Paramsol"
           nam3=""
           CALL sub_writeunstruct_paramsol_binary(nam2,nam3,-1)
        END IF
     ELSE
        IF(nflu.GT.0) THEN
           nam2="Paramflu"
           nam3=""
           CALL sub_writeunstruct3D_paramflu_binary(nam2,nam3,-1)
        END IF
        IF(nsol.GT.0) THEN
           nam2="Paramsol"
           nam3=""
           CALL sub_writeunstruct3D_paramsol_binary(nam2,nam3,-1)
        END IF
     END IF





     !initialisation of the vectors used for the communications
     CALL  sub_init_comm


     CALL  sub_calc_cfl

     CALL  sub_reshape_matrix
     
     CALL sub_init_cond

 




        !! Calcul Energie
!!$     IF(myrank.EQ.0)   OPEN(unit=34,file='energ.dat')


     DO I=1,nb_rcv
        WRITE(nam,*)Idx_rcv_glob(I)
        IF (Idx_rcv_glob(I).LE.9) THEN
           nam="000"//TRIM(ADJUSTL(nam))
        ELSEIF (Idx_rcv_glob(I).LE.99) THEN
           nam="00"//TRIM(ADJUSTL(nam))
        ELSEIF (Idx_rcv_glob(I).LE.999) THEN
           nam="0"//TRIM(ADJUSTL(nam))
        END IF
        OPEN(100+2*(I-1)+1,FILE="FILM/sismo_P."//TRIM(nam)//".dat")
        OPEN(100+2*(I-1)+2,FILE="FILM/sismo_U."//TRIM(nam)//".dat")
     ENDDO

CALL sub_time_loop
     DO I=1,nb_rcv
        CLOSE(100+2*(I-1)+1)
        CLOSE(100+2*(I-1)+2)
     ENDDO

     !     WRITE(6,*) U_new(1:30)
  END IF
  CALL mpi_finalize(ierr)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! Fin de l'ecriture des sismos et des snapshots
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
END PROGRAM hou10ni_helmholtz

