SUBROUTINE sub_CLA_curved
  USE PRECISION
  USE DATA
  USE mesh
  USE POLYNOM
  USE BASIS_FUNCTIONS
  USE Matrix
  USE mumps
  USE curved2
  USE pml
  USE constant
  USE transient
  USE MPI_modif
  IMPLICIT NONE

  INTEGER :: I,INeigh,Node(3),J,K,L,L1,L2,Q,Edge,NbnoCla,J1,J2,deg,isol,JJ,II&
       &,tmp_flu,tmp_sol,phiface(nverticesperface,nfacesperelem)
  REAL(kind=dp) :: V(3,2),TestCla,d,val
  REAL(kind=dp),ALLOCATABLE :: Ainter(:,:,:)  ,mat_aux(:,:)
  REAL(kind=dp) :: pt_courbe_x(Order_curved+1),pt_courbe_y(Order_curved+1)&
       &,dtheta
  REAL(kind=dp) :: angle,kappa,co,si,coeffi,a,b,c,nx,ny,vp1,vp2,vs,xg,yg,zg,xprime&
       &,yprime,xsecond,ysecond,curv,norme,vp,vs1,vs2
  COMPLEX(kind=dp) :: coeff_pml_x,coeff_pml_y,coeff_pml_z &
       &,coeff_pml_y_neigh,res,res1,res2,res3,res4,resxx,resxy,resyy
  COMPLEX(kind=dp) :: A_cla,iomega,coeff_cla,theta_cla,gamma_cla,zeta_cla, kappa_cla,&
       & Denominator_cla, Numerator_cla

  WRITE(6,*) 'DOES sub_CLA_curved.F90 EVER RUN ?'


  NBcla=0
  NBcla_flu=0
  NBcla_sol=0
  DO I=1,Ntri_loc
     DO J=1,nfacesperelem
        IF (Neigh_loc(I,J)==-3) THEN
           NbCLA=NbCLA+1
           IF(acouela(ref_media_loc(I)).EQ.1) THEN
              nbcla_flu=nbcla_flu+1
              tmp_flu=tmp_flu+Nphi(degree_tri_loc(I))**2
           ELSE 
              nbcla_sol=nbcla_sol+1
              tmp_sol=tmp_sol+dim**2*Nphi(degree_tri_loc(I))**2
           END IF
           EXIT
        END IF
     ENDDO
  ENDDO

  ALLOCATE(TriCla_flu(NbCLA_flu))
  ALLOCATE(TriCla_sol(NbCLA_sol))
  !allocate(Clatri(Ntri))
  ALLOCATE(BCla_flu(NbCla_flu))
  ALLOCATE(BCla_xx(NbCla_sol))
  ALLOCATE(BCla_yx(NbCla_sol))
  ALLOCATE(BCla_xy(NbCla_sol))
  ALLOCATE(BCla_yy(NbCla_sol))
  ALLOCATE(BCla_xx_tti(NbCla_sol))
  ALLOCATE(BCla_yx_tti(NbCla_sol))
  ALLOCATE(BCla_xy_tti(NbCla_sol))
  ALLOCATE(BCla_yy_tti(NbCla_sol))
  IF(helmholtz.EQ.0) THEN
     ALLOCATE(idx_mat_cla_flu(NbCla_flu+1))
     ALLOCATE(idx_mat_cla_sol(Nbcla_sol+1))
     idx_mat_cla_flu(1)=1
     idx_mat_cla_sol(1)=1
     ALLOCATE(A_CLA_flu(tmp_flu))
     ALLOCATE(A_CLA_sol(tmp_sol))
     A_CLA_flu=0.
     A_CLA_sol=0.
     ALLOCATE(mat_aux(dim*nphi(degree_max),dim*nphi(degree_max)))
  END IF
  NbCla=0
  NbCla_flu=0
  NbCla_sol=0
  NbCla=0
  NbnoCla=0
  DO I=1,NTri_loc
     Node = Tri_loc(I,:)
     ! FIXME : check that is necessary or not
     !!Computation of vectors V12,V23 and V31
     V(1,1)=Coor(Node(3),1)-Coor(Node(2),1)
     V(1,2)=Coor(Node(3),2)-Coor(Node(2),2)
     V(2,1)=Coor(Node(1),1)-Coor(Node(3),1)
     V(2,2)=Coor(Node(1),2)-Coor(Node(3),2)
     V(3,1)=Coor(Node(2),1)-Coor(Node(1),1)
     V(3,2)=Coor(Node(2),2)-Coor(Node(1),2)

     coeff_pml_y=1._dp
     coeff_pml_x=1._dp
     IF(ispml) THEN
        xg=SUM(Coor(Node(:),1))/3D0
        yg=SUM(Coor(Node(:),2))/3D0
        IF(pmlayer%ispmlbottom) THEN
           IF(yg.LE.pmlayer%yb) THEN
              coeff_pml_y=dcmplx(0,omega)/(dcmplx(0,omega)+(yg-pmlayer%yb)**2 &
                   &*pmlayer%coeff_b)
           END IF
        END IF
        IF(pmlayer%ispmltop) THEN
           IF(yg.GE.pmlayer%yt) THEN
              coeff_pml_y=dcmplx(0,omega)/(dcmplx(0,omega)+(yg-pmlayer%yt)**2 &
                   &*pmlayer%coeff_t)
           END IF
        END IF
        IF(pmlayer%ispmlleft) THEN
           IF(xg.LE.pmlayer%xl) THEN
              coeff_pml_x=dcmplx(0,omega)/(dcmplx(0,omega)+(xg-pmlayer%xl)**2 &
                   &*pmlayer%coeff_l)
           END IF
        END IF
        IF(pmlayer%ispmlright) THEN
           IF(xg.GE.pmlayer%xr) THEN
              coeff_pml_x=dcmplx(0,omega)/(dcmplx(0,omega)+(xg-pmlayer%xr)**2 &
                   &*pmlayer%coeff_r)
           END IF
        END IF
     END IF
     TestCla=0
     DO Edge=1,Nfacesperelem
!!! Searching Neighbor
        INeigh=Neigh_loc(I,Edge)
!!! If there is no Neighbor and the edge carries a ABC
        IF(Ineigh.EQ.-3) THEN
           IF (TestCla==0) THEN
              TestCla=1
              NbCla=NbCla+1
              IF(acouela(ref_media_loc(I)).EQ.1) THEN
                 nbcla_flu=nbcla_flu+1
                 TriCla_flu(NbCla_flu)=I
                 IF(helmholtz.EQ.1) THEN
                    ALLOCATE(Bcla_flu(NbCla_flu)&
                         &%coeff(nphi(degree_tri_loc(I)) &
                         &,nphi(degree_tri_loc(I))))
                    Bcla_flu(NbCla_flu)%coeff=0
                 ELSE 
                    tmp_flu=tmp_flu+Nphi(degree_tri_loc(I))**2
                    idx_mat_cla_flu(Nbcla_flu+1)=tmp_flu+1
                 END IF
              ELSE 
                 nbcla_sol=nbcla_sol+1
                 TriCla_sol(NbCla_sol)=I

                 IF(helmholtz.EQ.1) THEN
                    ALLOCATE(Bcla_sol(NbCla_sol)%coeff(dim&
                         &*nphi(degree_tri_loc(I)) &
                         &,dim*nphi(degree_tri_loc(I))))
                    Bcla_sol(NbCla_sol)%coeff=0
                 ELSE 
                    tmp_sol=tmp_sol+dim**2*Nphi(degree_tri_loc(I))**2
                    idx_mat_cla_sol(Nbcla_sol+1)=tmp_sol+1

                 END IF
                 IF(dim.EQ.2) THEN
                    Isol=I-Nflu_loc-Nflusol_loc

                    IF(ABS(Cij(Isol,1,3)+Cij(Isol,2,3)).GT.1e-8) THEN
                       angle= ACOS((Cij(Isol,1,1)-Cij(Isol,2,2))/2. &
                            &/SQRT((Cij(Isol,1,3)+Cij(Isol,2,3))**2 +(Cij(Isol &
                            &,1,1)-Cij(Isol,2,2))**2/4.))/2.
                    ELSE
                       angle=0.
                    END IF
                    kappa=SQRT((COS(angle)**4*Cij(Isol,1,1)+SIN(angle)**4*Cij(Isol,2 &
                         &,2)+2.*COS(angle)**2 *SIN(angle)**2*(2.*Cij(Isol,3,3) &
                         &+Cij(Isol,1,2))+4.*COS(angle)**3 *SIN(angle)*Cij(Isol,1,3) &
                         &+4.*COS(angle) *SIN(angle)**3*Cij(Isol,2,3))/(SIN(angle)**4&
                         & *Cij(Isol,1,1)+COS(angle)**4*Cij(Isol,2,2)+2.*COS(angle)&
                         &**2 *SIN(angle)**2*(2.*Cij(Isol,3,3)+Cij(Isol,1,2))-4. &
                         &*SIN(angle)**3 *COS(angle)*Cij(Isol,1,3)-4.*SIN(angle) &
                         &*COS(angle)**3*Cij(Isol,2,3)))
                    vp=SQRT((SIN(angle)**4*Cij(Isol,1,1)+COS(angle)**4*Cij(Isol &
                         &,2,2)+2.*COS(angle)**2 *SIN(angle)**2*(2.*Cij(Isol,3,3)&
                         & +Cij(Isol,1,2))-4.*SIN(angle)**3 *COS(angle)*Cij(Isol&
                         & ,1,3)-4.*SIN(angle) *COS(angle)**3*Cij(Isol,2,3)) &
                         &/rho(I))
                    vs1=SQRT((COS(angle)**2*SIN(angle)**2*(Cij(Isol,1,1)+Cij(Isol&
                         & ,2,2))+(COS(angle)**2 -SIN(angle)**2)**2*Cij(Isol,3,3)&
                         & -2.*COS(angle)* SIN(angle)*(COS(angle)**2-SIN(angle)&
                         &**2) *(Cij(Isol,1,3)-Cij(Isol,2,3))-2.*COS(angle)**2 &
                         &*SIN(angle)**2*Cij(Isol,1,2))/rho(I))
                 ELSE 
                    Isol=I-Nflu_loc-Nflusol_loc
                    vp=SQRT(Cij(Isol,1,1)/rho(I))
                    vs1=SQRT(Cij(Isol,4,4)/rho(I))
                    vs2=SQRT(Cij(Isol,4,4)/rho(I))
                 END IF
              END IF
              IF(helmholtz.EQ.1) THEN
                 coeff_pml_x=1.0
                 coeff_pml_y=1.0
                 IF(ispml) THEN
                    xg=SUM(Coor(Tri_loc(I,:),1))/REAL(Nverticesperelem,dp)
                    yg=SUM(Coor(Tri_loc(I,:),2))/REAL(Nverticesperelem,dp)
                    IF(dim.EQ.2) THEN
                       IF(pmlayer%ispmlbottom) THEN
                          IF(yg.LE.pmlayer%yb) THEN
                             coeff_pml_y=dcmplx(0,omega)/(dcmplx(0,omega)&
                                  &+(yg-pmlayer&
                                  &%yb)**2 &
                                  &*pmlayer%coeff_b)
                          END IF
                       END IF
                       IF(pmlayer%ispmltop) THEN
                          IF(yg.GE.pmlayer%yt) THEN
                             coeff_pml_y=dcmplx(0,omega)/(dcmplx(0,omega)&
                                  &+(yg-pmlayer&
                                  &%yt)**2 &
                                  &*pmlayer%coeff_t)
                          END IF
                       END IF
                       IF(pmlayer%ispmlleft) THEN
                          IF(xg.LE.pmlayer%xl) THEN
                             coeff_pml_x=dcmplx(0,omega)/(dcmplx(0,omega)&
                                  &+(xg-pmlayer&
                                  &%xl)**2 &
                                  &*pmlayer%coeff_l)
                          END IF
                       END IF
                       IF(pmlayer%ispmlright) THEN
                          IF(xg.GE.pmlayer%xr) THEN
                             coeff_pml_x=dcmplx(0,omega)/(dcmplx(0,omega)+(xg&
                                  &-pmlayer&
                                  &%xr)**2 &
                                  &*pmlayer%coeff_r)
                          END IF
                       END IF
                    ELSE 
                       zg=SUM(Coor(Tri_loc(I,:),1))/REAL(Nverticesperelem,dp)
                       IF(pmlayer%ispmlbottom) THEN
                          IF(zg.LE.pmlayer%zb) THEN
                             coeff_pml_z=dcmplx(0,omega)/(dcmplx(0,omega)+(zg&
                                  &-pmlayer%zb)**2 &
                                  &*pmlayer%coeff_b)
                          END IF
                       END IF
                       IF(pmlayer%ispmltop) THEN
                          IF(zg.GE.pmlayer%zt) THEN
                             coeff_pml_z=dcmplx(0,omega)/(dcmplx(0,omega)+(zg&
                                  &-pmlayer%zt)**2 &
                                  &*pmlayer%coeff_t)
                          END IF
                       END IF
                       IF(pmlayer%ispmlback) THEN
                          IF(yg.LE.pmlayer%yb) THEN
                             coeff_pml_y=dcmplx(0,omega)/(dcmplx(0,omega)+(yg&
                                  &-pmlayer%yb)**2 &
                                  &*pmlayer%coeff_ba)
                          END IF
                       END IF
                       IF(pmlayer%ispmlfore) THEN
                          IF(yg.GE.pmlayer%yt) THEN
                             coeff_pml_y=dcmplx(0,omega)/(dcmplx(0,omega)+(yg&
                                  &-pmlayer%yt)**2 &
                                  &*pmlayer%coeff_fo)
                          END IF
                       END IF
                       IF(pmlayer%ispmlleft) THEN
                          IF(xg.LE.pmlayer%xl) THEN
                             coeff_pml_x=dcmplx(0,omega)/(dcmplx(0,omega)+(xg&
                                  &-pmlayer%xl)**2 &
                                  &*pmlayer%coeff_l)
                          END IF
                       END IF
                       IF(pmlayer%ispmlright) THEN
                          IF(xg.GE.pmlayer%xr) THEN
                             coeff_pml_x=dcmplx(0,omega)/(dcmplx(0,omega)+(xg&
                                  &-pmlayer%xr)**2 &
                                  &*pmlayer%coeff_r)
                          END IF
                       END IF
                    END IF
                 END IF
              END IF
           END IF
           !!Computation of vectors V12,V23 and V31
           V(1,1)=Coor(Node(3),1)-Coor(Node(2),1)
           V(1,2)=Coor(Node(3),2)-Coor(Node(2),2)
           V(2,1)=Coor(Node(1),1)-Coor(Node(3),1)
           V(2,2)=Coor(Node(1),2)-Coor(Node(3),2)
           V(3,1)=Coor(Node(2),1)-Coor(Node(1),1)
           V(3,2)=Coor(Node(2),2)-Coor(Node(1),2)
           SELECT CASE(Edge)
           CASE(1)
              J1=2
              J2=3
           CASE(2)
              J1=3
              J2=1
           CASE(3)
              J1=1
              J2=2
           END SELECT
           pt_courbe_x(1)=Coor(Node(J1),1)
           pt_courbe_x(order_curved+1)=Coor(Node(J2),1)
           pt_courbe_y(1)=Coor(Node(J1),2)
           pt_courbe_y(order_curved+1)=Coor(Node(J2),2)



           CALL sub_def_pt_courbe_1d(pt_courbe_x,pt_courbe_y,Triloc2glob(I),Edge)


           deg=degree_tri_loc(I)
           IF(acouela(ref_media_loc(I)).EQ.1) THEN
              IF(helmholtz.EQ.1) THEN
                 !! [u][v]
                 DO J=1,nphi(deg)
                    DO K=1,nphi(deg)
                       res=0D0
                       DO L=1,NGL1D
                          !! computation of the curvature 'curv' for the dof
                          xprime=SUM(pt_courbe_x*value_basis_curved_1D%value_volume(1)&
                               &%matphii_ptgl(2)%coeff(L,:,1))

                          yprime=SUM(pt_courbe_y*value_basis_curved_1D%value_volume(1)&
                               &%matphii_ptgl(2)%coeff(L,:,1))
                          norme=SQRT(xprime**2+yprime**2)
                          xsecond=SUM(pt_courbe_x*value_basis_curved_1D%value_volume(1)&
                               &%matphii_ptgl(3)%coeff(L,:,1))

                          ysecond=SUM(pt_courbe_y*value_basis_curved_1D%value_volume(1)&
                               &%matphii_ptgl(3)%coeff(L,:,1))
                          curv=(xprime*ysecond/norme**3-yprime*xsecond/norme**3)
                          SELECT CASE(type_cla)
                          CASE(1)
                             coeff_cla =  (dcmplx(0,omega))/sqrt(mu(I)*rho(I))
                          CASE(2)
                             coeff_cla = dcmplx(curv/(2._dp*rho(I)),omega/sqrt(mu(I)*rho(I)))
                          CASE(10)
                             !! MODIF BY Andy 
                             iomega = -dcmplx(0,omega)*sqrt(rho(I))/sqrt(mu(I))
                             kappa_cla = curv
                             ! WRITE(6,*) 'courbure = ', kappa_cla
                             ! YOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
                             theta_cla = theta_cla_0 * kappa_cla
                             gamma_cla = gamma_cla_0 * kappa_cla
                             zeta_cla = zeta_cla_0 * kappa_cla
                             ! YOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
                             A_cla = kappa_cla/(2.0_16*iomega)*(kappa_cla/2.0_16-gamma_cla)
                             Numerator_cla = iomega*((iomega+theta_cla) * (-iomega - gamma_cla-kappa_cla/4.0_16) &
                                  & +A_cla/2.0_16 *( iomega+zeta_cla -kappa_cla/4.0_16 ) )
                             Denominator_cla = ( (iomega+theta_cla) * (-iomega - gamma_cla+kappa_cla/4.0_16) &
                                  & +A_cla/2.0_16 *( iomega-zeta_cla - kappa_cla/4.0_16 ))

                             coeff_cla = CONJG((Numerator_cla)/(Denominator_cla)/(rho(I)))

                             !Mat_pp_cmplx(1:Nphi(deg),1:Nphi(deg))=Mat_pp_cmplx(1:Nphi(deg)&
                             ! &,1:Nphi(deg))+coef_cla*Bcla_flu(Icla(tid+1))%coeff/(rho(I))

                          END SELECT

                          res=res+value_basis%value_surface(deg)&
                               &%matphii_ptgl_surf(1)%coeff(L,J,1,Edge)*value_basis&
                               &%value_surface(deg)%matphii_ptgl_surf(1)&
                               &%coeff(L,K,1,Edge)*wGL1D(L)*norme*coeff_cla

                       ENDDO
                       !write(6,*) res,sqrt(sum(V(Edge,:)**2))
                       Bcla_flu(NbCla_flu)%coeff(J,K)=Bcla_flu(NbCla_flu)%coeff(J,K)+res/coeff_pml_x/coeff_pml_y
                    END DO
                 END DO
              ELSE
                 !! TODO : multiplication by Minv curved
              END IF
           ELSE 
              !! [u][v]

              Isol=I-Nflu-Nflusol
              DO J=1,nphi(deg)
                 DO K=1,nphi(deg)
                    resxx=0D0
                    resxy=0D0
                    resyy=0D0
                    res1=0D0
                    res2=0D0
                    res3=0D0
                    res4=0D0
                    DO L=1,NGL1D
                       ny=-SUM(pt_courbe_x*value_basis_curved_1D%value_volume(1)&
                            &%matphii_ptgl(2)%coeff(L,:,1))
                       nx=SUM(pt_courbe_y*value_basis_curved_1D%value_volume(1)&
                            &%matphii_ptgl(2)%coeff(L,:,1))
                       norme=SQRT(nx**2+ny**2)
                       co=COS(angle)*nx+SIN(angle)*ny
                       si=-COS(angle)*ny+SIN(angle)*nx

                       a=(kappa*co**2+si**2)**2/SQRT(kappa**2*co**2+si**2)
                       b=(kappa*co**2+si**2)*(kappa-1.)*co*si/SQRT(kappa**2*co**2+si**2)
                       c=(kappa*co**2+si**2)*(kappa-1.)*co*si/SQRT(kappa**2*co**2+si**2)
                       d=(kappa-1.)**2*co**2*si**2/SQRT(kappa**2*co**2+si**2)
                       val=value_basis%value_surface(deg)&
                            &%matphii_ptgl_surf(1)%coeff(L,J,1,Edge)*value_basis&
                            &%value_surface(deg)%matphii_ptgl_surf(1)&
                            &%coeff(L,K,1,Edge)*wGL1D(L)
                       IF(helmholtz.EQ.1) THEN

                          coeffi=a*nx**2+d*ny**2-(b+c)*nx*ny
                          Bcla_sol(NbCla_sol)%coeff(1:nphi(degree_tri_loc(I))&
                               &,1:nphi(degree_tri_loc(I)))=&
                               &Bcla_sol(NbCla_sol)&
                               &%coeff(1:nphi(degree_tri_loc(I))&
                               &,1:nphi(degree_tri_loc(I)))-&
                               &dcmplx(0,omega)*rho(I)*val*&
                               &(coeffi*Vp+ny**2*Vs1)&
                               &/norme/coeff_pml_x/coeff_pml_y

                          coeffi=c*nx**2-b*ny**2+(a-d)*nx*ny

                          Bcla_sol(NbCla_sol)%coeff(1:nphi(degree_tri_loc(I))&
                               &,nphi(degree_tri_loc(I))+1:2&
                               &*nphi(degree_tri_loc(I)))=&
                               &Bcla_sol(NbCla_sol)&
                               &%coeff(1:nphi(degree_tri_loc(I))&
                               &,nphi(degree_tri_loc(I))+1:2&
                               &*nphi(degree_tri_loc(I)))+&
                               &dcmplx(0,omega)*rho(I)*val*&
                               &(coeffi*Vp+nx*ny*Vs1)&
                               &/norme/coeff_pml_x/coeff_pml_y

                          coeffi=b*nx**2-c*ny**2+(a-d)*nx*ny

                          Bcla_sol(NbCla_sol)%coeff(nphi(degree_tri_loc(I))+1:2&
                               &*nphi(degree_tri_loc(I))&
                               &,1:nphi(degree_tri_loc(I)))=&
                               &Bcla_sol(NbCla_sol)&
                               &%coeff(nphi(degree_tri_loc(I))+1:2*nphi(degree_tri_loc(I))&
                               &,1:nphi(degree_tri_loc(I)))-&
                               &dcmplx(0,omega)*rho(I)*val*&
                               &(coeffi*Vp+nx*ny*Vs1)&
                               &/norme/coeff_pml_x/coeff_pml_y

                          coeffi=d*nx**2+a*ny**2+(b+c)*nx*ny

                          Bcla_sol(NbCla_sol)%coeff(nphi(degree_tri_loc(I))+1:2&
                               &*nphi(degree_tri_loc(I))&
                               &,nphi(degree_tri_loc(I))+1:2&
                               &*nphi(degree_tri_loc(I)))=&
                               &Bcla_sol(NbCla_sol)&
                               &%coeff(nphi(degree_tri_loc(I))+1:2&
                               &*nphi(degree_tri_loc(I))&
                               &,nphi(degree_tri_loc(I))+1:2&
                               &*nphi(degree_tri_loc(I)))-&
                               &dcmplx(0,omega)*rho(I)*val*&
                               &(coeffi*Vp+nx**2*Vs1)&
                               &/norme/coeff_pml_x/coeff_pml_y


                       ELSE 
                          WRITE(6,*) 'todo abc for curved boundaries in time domain'
                          STOP
                       END IF
                    END DO
                 END DO
              END DO
           END IF
        END IF
        IF (TestCla==0) THEN
           NbnoCla=NbnoCla+1
        END IF
     END DO

  END DO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!    
!!!!!!!!! Computation of Minv*C
!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!TODO
!!$IF (helmholtz.EQ.0) THEN
!!$DO I=1,Nbcla
!!$   Bcla(I,1:Nphi,1:Nphi)=MATMUL(Minv,Bcla(I,1:Nphi,1:Nphi))/DFVec(TriCla(I))*mu(TriCla(I))
!!$ENDDO
!!$END IF

END SUBROUTINE sub_CLA_curved
