SUBROUTINE sub_rhs3D_acoustoneumann
  USE PRECISION
  USE constant
  USE DATA
  USE mesh
  USE mumps
  USE MPI_MODIF

  IMPLICIT NONE
  TYPE vec_cmplx
     COMPLEX(kind=dp), DIMENSION(:), ALLOCATABLE ::VALUE
  END TYPE vec_cmplx
  TYPE vec_int
     INTEGER, DIMENSION(:), ALLOCATABLE ::VALUE
  END TYPE vec_int
  TYPE(vec_cmplx) :: idrhs_SPARSE_master(nproc)
  TYPE(vec_int) :: idirhs_SPARSE_master(nproc)
  REAL(kind=dp),DIMENSION(:),ALLOCATABLE :: vectJ
  COMPLEX(kind=dp), DIMENSION(:), ALLOCATABLE ::Valsourcebord,idref
  COMPLEX(kind=dp), DIMENSION(:,:), ALLOCATABLE ::Gradvalsourcebord,GradGradvalsourcebord
  COMPLEX(kind=dp) :: GradPhiIG(3),GradGradPhiIG(6)

  REAL(kind=dp) :: V(5,3),Jfinv(3,3),thetai(3),norm(4,3),h2,hmin,coor_x,coor_y&
       &,theta
  REAL(kind=dp) :: coorx1,coorx2,coorx3,coory1,coory2,coory3,coorz1&
       &,coorz2,coorz3,cmax,dfvec,vp,vs
  INTEGER :: Node(4),I,int_j,J,l,NsommeNum,K,NCALC,Ineigh,Nsomme,deg,deg_neigh&
       &,ind,idnz_rhs_master(nproc),TAG(3),procI


  REAL(kind=dp) :: demi_grand_axe_a,demi_petit_axe_b,h1,ksi,foc

  REAL(kind=dp) :: coord_points(4),d

  DO I=1,3
     TAG(I) = I
  END DO

  ALLOCATE(Valsourcebord(NGL2D))
  ALLOCATE(GradValsourcebord(NGL2D,3))

  id%nrhs = 1
  id%NZ_RHS = 0
  ind = 0

  DO J=1,Nflu_loc+Nflusol_loc
     DO int_j=1,4
        INeigh = Neigh_loc(J,int_j)   
        !! If the face of the current element J is an exterior face with a Neuman Boundary Condition (-1)
        IF(Ineigh.EQ.-1) THEN
           id%NZ_RHS = id%NZ_RHS + Nphi(degree_tri_loc(J))
        END IF
     END DO
  END DO

  ALLOCATE(id%RHS_SPARSE(id%NZ_RHS))
  ALLOCATE(id%IRHS_SPARSE(id%NZ_RHS))
  ALLOCATE(id%IRHS_PTR(2))

  id%lrhs = index_tri(Ntri)
  id%IRHS_PTR(1) = 1
  id%IRHS_PTR(2) = id%NZ_RHS+1
  id%RHS_SPARSE = 0


  !----------------------------------------------------------------------------
  !* Loop on all the elements of the fluid domain
  !----------------------------------------------------------------------------
  DO J=1,Nflu_loc+Nflusol_loc

     Node = Tri_loc(J,:)

     !! Computation of the vectors constituing the edges of the tetraedron
     !Nodes :
     !.4            
     !|\ .        
     !| \  .      
     !|  \    .3 
     !|   \  /
     !|____\/2
     !1
     V(1,:) = Coor(Node(2),:)-Coor(Node(1),:)
     V(2,:) = Coor(Node(3),:)-Coor(Node(1),:)
     V(3,:) = Coor(Node(4),:)-Coor(Node(1),:)
     V(4,:) = Coor(Node(3),:)-Coor(Node(2),:)
     V(5,:) = Coor(Node(4),:)-Coor(Node(2),:)


     !! Computation of the normal vectors of the 4 faces

     !! NOTE We do not normalize the external normal vectors. So we will not 
     !!      multiply the integral obtained on the reference element by
     !!      the norm of this vector.

     !! Face 1
     norm(1,1) = V(4,2)*V(5,3)-V(4,3)*V(5,2)
     norm(1,2) = V(4,3)*V(5,1)-V(4,1)*V(5,3)
     norm(1,3) = V(4,1)*V(5,2)-V(4,2)*V(5,1)
     !!     norm(1,:) = norm(1,:)/sqrt(sum(norm(1,:)**2))
     !! Face 2
     norm(2,1) = V(3,2)*V(2,3)-V(3,3)*V(2,2)
     norm(2,2) = V(3,3)*V(2,1)-V(3,1)*V(2,3)
     norm(2,3) = V(3,1)*V(2,2)-V(3,2)*V(2,1)
     !!     norm(2,:) = norm(2,:)/sqrt(sum(norm(2,:)**2))
     !! Face 3
     norm(3,1) = V(1,2)*V(3,3)-V(1,3)*V(3,2)
     norm(3,2) = V(1,3)*V(3,1)-V(1,1)*V(3,3)
     norm(3,3) = V(1,1)*V(3,2)-V(1,2)*V(3,1)
     !!     norm(3,:) = norm(3,:)/sqrt(sum(norm(3,:)**2))
     !! Face 4
     norm(4,1) = V(2,2)*V(1,3)-V(2,3)*V(1,2)
     norm(4,2) = V(2,3)*V(1,1)-V(2,1)*V(1,3)
     norm(4,3) = V(2,1)*V(1,2)-V(2,2)*V(1,1)
     !!     norm(4,:) = norm(4,:)/sqrt(sum(norm(4,:)**2))

     deg = degree_tri_loc(J)


     !----------------------------------------------------------------------------
     !** Loop on all the faces of the current element
     !----------------------------------------------------------------------------
     DO int_j=1,4
        INeigh = Neigh_loc(J,int_j) !! Num of the neighbourgh of the current element J on the face int_j
        !! If the face of the current element J is an exterior face with a Neuman Boundary Condition (-1)
        IF(Ineigh.EQ.-1) THEN
           !----------------------------------------------------------------------------
           !*** Computation of the 3 nodes of the current face
           !----------------------------------------------------------------------------

           SELECT CASE(int_j)
              !! Face 1
           CASE(1)
              coorx1 = Coor(Node(2),1)
              coory1 = Coor(Node(2),2)
              coorz1 = Coor(Node(2),3)
              coorx2 = Coor(Node(4),1)
              coory2 = Coor(Node(4),2)
              coorz2 = Coor(Node(4),3)
              coorx3 = Coor(Node(3),1)
              coory3 = Coor(Node(3),2)
              coorz3 = Coor(Node(3),3)
              !! Face 2
           CASE(2)
              coorx1 = Coor(Node(1),1)
              coory1 = Coor(Node(1),2)
              coorz1 = Coor(Node(1),3)
              coorx2 = Coor(Node(3),1)
              coory2 = Coor(Node(3),2)
              coorz2 = Coor(Node(3),3)
              coorx3 = Coor(Node(4),1)
              coory3 = Coor(Node(4),2)
              coorz3 = Coor(Node(4),3)
              !! Face 3
           CASE(3)
              coorx1 = Coor(Node(1),1)
              coory1 = Coor(Node(1),2)
              coorz1 = Coor(Node(1),3)
              coorx2 = Coor(Node(4),1)
              coory2 = Coor(Node(4),2)
              coorz2 = Coor(Node(4),3)
              coorx3 = Coor(Node(2),1)
              coory3 = Coor(Node(2),2)
              coorz3 = Coor(Node(2),3)
              !! Face 4
           CASE(4)
              coorx1 = Coor(Node(1),1)
              coory1 = Coor(Node(1),2)
              coorz1 = Coor(Node(1),3)
              coorx2 = Coor(Node(2),1)
              coory2 = Coor(Node(2),2)
              coorz2 = Coor(Node(2),3)
              coorx3 = Coor(Node(3),1)
              coory3 = Coor(Node(3),2)
              coorz3 = Coor(Node(3),3)
           END SELECT

           CALL sub_ray_sphere_ins(Coor(Node(1),:),Coor(Node(2),:),Coor(Node(3) &
                &,:),Coor(Node(4),:),h1) 

           vp = SQRT(mu(J)/rho(J)) !! Velocity of the wave in the current element

           !!           d=SQRT(SUM(norm(int_j,:)**2)) 

           !----------------------------------------------------------------------------
           !*** Computation of the rhs term int_GammaExt(g * phi_j) 
           !----------------------------------------------------------------------------
           !! IDEA For Plane waves g = d_n(exp(iw/c(..)))
           !! IDEA We use a quadrature formula : int_GammaExt(g * phi_j) = sum_l(g(x_l) * phi_j(x_l) * w_l)
           !! NOTE g = exp(iw/c(..)) for plane waves

           !----------------------------------------------------------------------------
           !**** Loop on the nodes of the quadrature 
           !----------------------------------------------------------------------------

           DO l=1,NGL2D
              !----------------------------------------------------------------------------
              !***** Value of exp(iw/c(..)) at the quadrature node l
              !----------------------------------------------------------------------------

              Valsourcebord(l) = EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+& 
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              !----------------------------------------------------------------------------
              !***** Value of grad(exp(iw/c(..)))  at the quadrature node l
              !----------------------------------------------------------------------------

              GradValsourcebord(l,1) = -CMPLX(0,omega/Vp)*COS(theta0)*COS(theta1)&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              GradValsourcebord(l,2) = -CMPLX(0,omega/Vp)*SIN(theta0)*COS(theta1)&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              GradValsourcebord(l,3) = -CMPLX(0,omega/Vp)*SIN(theta1)&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))
           ENDDO

           !----------------------------------------------------------------------------
           !**** Loop on the basis funtions defined on the current element
           !----------------------------------------------------------------------------

           DO K=1,nphi(degree_tri_loc(J))
              !----------------------------------------------------------------------------
              !***** Computation of each component (indexed by l) of sum_{X_q}{grad(exp(iw/c(..)))*phi_j(X_q)*w_q}
              !----------------------------------------------------------------------------
              DO l=1,dim
                 GradPhiIG(l) = SUM(value_basis%value_surface(degree_tri_loc(J))&
                      &%matphii_ptgl_surf(1)%coeff(:,K,1,int_j)*GradValsourcebord(:,l)*wgl2D(:))
              END DO
              !----------------------------------------------------------------------------
              !***** Scalar product between the vector sum_{X_q}{grad(exp(iw/c(..)))*phi_j(X_q)*w_q} and the external normal vector n
              !----------------------------------------------------------------------------
              !! If we normalize the normal vectors : 
              !!              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
              !!                                                          &d*SUM(GradPhiIG*norm(int_j,:))/rho(J)
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &SUM(GradPhiIG(:)*norm(int_j,:))/rho(J)

              !TODO Check the "d*" OK ,
              !TODO no use of Jinv OK
              !TODO Check the "+" , "-" was OK A PRIORI
              !TODO Check the "rho(I)" OK


           END DO

           ind=ind+nphi(degree_tri_loc(J))
        END IF
     END DO !! End of the loop on the faces of the curent element
  END DO !! End of the loop on the elements

  ALLOCATE(idref(id%N))

  CALL MPI_ALLREDUCE(id%rhs,idref,id%N, MPI_COMPLEX_PERSO,MPI_sum&
       &,MPI_COMM_WORLD,ierr)

  id%rhs = idref
  !TODO Check ? I have deleted the /2. and now it seems to work for plane waves OK

END SUBROUTINE sub_rhs3D_acoustoneumann




SUBROUTINE sub_rhs3D_elastoneumann
  USE PRECISION
  USE constant
  USE DATA
  USE mesh
  USE mumps
  USE MPI_MODIF
  IMPLICIT NONE
  TYPE vec_cmplx
     COMPLEX(kind=dp), DIMENSION(:), ALLOCATABLE ::VALUE
  END TYPE vec_cmplx
  TYPE vec_int
     INTEGER, DIMENSION(:), ALLOCATABLE ::VALUE
  END TYPE vec_int
  TYPE(vec_cmplx) :: idrhs_SPARSE_master(nproc)
  TYPE(vec_int) :: idirhs_SPARSE_master(nproc)
  REAL(kind=dp),DIMENSION(:),ALLOCATABLE :: vectJ
  COMPLEX(kind=dp), DIMENSION(:), ALLOCATABLE ::Valsourcebord,idref
  COMPLEX(kind=dp), DIMENSION(:,:), ALLOCATABLE ::Gradvalsourcebord,GradGradvalsourcebord
  COMPLEX(kind=dp) :: GradPhiIG(3),GradGradPhiIG(6)

  REAL(kind=dp) :: V(5,3),Jfinv(3,3),thetai(3),norm(4,3),h2,hmin,coor_x,coor_y&
       &,theta
  REAL(kind=dp) :: coorx1,coorx2,coorx3,coory1,coory2,coory3,coorz1&
       &,coorz2,coorz3,cmax,dfvec,vp,vs
  INTEGER :: Node(4),I,int_j,J,l,NsommeNum,K,NCALC,Ineigh,Nsomme,deg,deg_neigh&
       &,ind,idnz_rhs_master(nproc),TAG(3),procI


  REAL(kind=dp) :: demi_grand_axe_a,demi_petit_axe_b,h1,ksi,foc

  REAL(kind=dp) :: coord_points(4),d

  DO I=1,3
     TAG(I)=I
  END DO
  ALLOCATE(Valsourcebord(NGL2D))
  ALLOCATE(GradValsourcebord(NGL2D,3))
  ALLOCATE(GradGradValsourcebord(NGL2D,6))

  !id%icntl(20)=1
  id%nrhs=1
  id%NZ_RHS=0
  ind=0

  !----------------------------------------------------------------------------
  !* Allocations 
  !----------------------------------------------------------------------------
  DO J=Nflu_loc+Nflusol_loc+1,Ntri_loc

     DO int_j=1,4
        INeigh=Neigh_loc(J,int_j)   
        !! Si le triangle est sur l'interface  
        IF(Ineigh.EQ.-1) THEN
           id%NZ_RHS=id%NZ_RHS+dim*Nphi(degree_tri_loc(J))
        END IF
     END DO
  END DO

  ALLOCATE(id%RHS_SPARSE(id%NZ_RHS))
  ALLOCATE(id%IRHS_SPARSE(id%NZ_RHS))
  ALLOCATE(id%IRHS_PTR(2))
  id%lrhs=index_tri(Ntri)
  id%IRHS_PTR(1)=1
  id%IRHS_PTR(2)=id%NZ_RHS+1
  id%RHS_SPARSE=0

  !----------------------------------------------------------------------------
  !* Loop on all the elements of the solid domain
  !----------------------------------------------------------------------------
  DO J=Nflu_loc+Nflusol_loc+1,Ntri_loc

     Node = Tri_loc(J,:)

     !!Computation of vectors V12,V23 and V31
     V(1,:)=Coor(Node(2),:)-Coor(Node(1),:)
     V(2,:)=Coor(Node(3),:)-Coor(Node(1),:)
     V(3,:)=Coor(Node(4),:)-Coor(Node(1),:)
     V(4,:)=Coor(Node(3),:)-Coor(Node(2),:)
     V(5,:)=Coor(Node(4),:)-Coor(Node(2),:)


     !!Computation of normal vectors n1,n2 and n3
     norm(1,1)=V(4,2)*V(5,3)-V(4,3)*V(5,2)
     norm(1,2)=V(4,3)*V(5,1)-V(4,1)*V(5,3)
     norm(1,3)=V(4,1)*V(5,2)-V(4,2)*V(5,1)
     !     norm(1,:)=norm(1,:)/sqrt(sum(norm(1,:)**2))
     norm(2,1)=V(3,2)*V(2,3)-V(3,3)*V(2,2)
     norm(2,2)=V(3,3)*V(2,1)-V(3,1)*V(2,3)
     norm(2,3)=V(3,1)*V(2,2)-V(3,2)*V(2,1)
     !     norm(2,:)=norm(2,:)/sqrt(sum(norm(2,:)**2))
     norm(3,1)=V(1,2)*V(3,3)-V(1,3)*V(3,2)
     norm(3,2)=V(1,3)*V(3,1)-V(1,1)*V(3,3)
     norm(3,3)=V(1,1)*V(3,2)-V(1,2)*V(3,1)
     !     norm(3,:)=norm(3,:)/sqrt(sum(norm(3,:)**2))
     norm(4,1)=V(2,2)*V(1,3)-V(2,3)*V(1,2)
     norm(4,2)=V(2,3)*V(1,1)-V(2,1)*V(1,3)
     norm(4,3)=V(2,1)*V(1,2)-V(2,2)*V(1,1)
     !     norm(4,:)=norm(4,:)/sqrt(sum(norm(4,:)**2))

     !! Computation of JFinv and DF
     DFVEC=-(V(2,2)*V(1,3)-V(2,3)*V(1,2))*V(3,1)-(V(2,3)*V(1,1)-V(2,1)*V(1,3))&
          &*V(3,2)-(V(2,1)*V(1,2)-V(2,2)*V(1,1))*V(3,3)
!!! Attention : Jfinv est la transposée de JF^-1
     Jfinv(1,1)=V(2,2)*V(3,3)-V(2,3)*V(3,2)
     Jfinv(1,2)=V(3,2)*V(1,3)-V(3,3)*V(1,2)
     Jfinv(1,3)=V(1,2)*V(2,3)-V(1,3)*V(2,2)
     Jfinv(2,1)=V(2,3)*V(3,1)-V(2,1)*V(3,3)
     Jfinv(2,2)=V(3,3)*V(1,1)-V(3,1)*V(1,3)
     Jfinv(2,3)=V(1,3)*V(2,1)-V(1,1)*V(2,3)
     Jfinv(3,1)=V(2,1)*V(3,2)-V(2,2)*V(3,1)
     Jfinv(3,2)=V(3,1)*V(1,2)-V(3,2)*V(1,1)
     Jfinv(3,3)=V(1,1)*V(2,2)-V(1,2)*V(2,1)
     Jfinv=(Jfinv/DFVEC)
     deg=degree_tri_loc(J)

     !----------------------------------------------------------------------------
     !** Loop on all the faces of the current element
     !----------------------------------------------------------------------------
     DO int_j=1,4
        INeigh=Neigh_loc(J,int_j)   
        !! Si le triangle est sur l'interface  
        IF(Ineigh.EQ.-1) THEN

           !----------------------------------------------------------------------------
           !*** Computation of the 3 nodes of the current face
           !----------------------------------------------------------------------------
           SELECT CASE(int_j)
           CASE(1)
              coorx1=Coor(Node(2),1)
              coory1=Coor(Node(2),2)
              coorz1=Coor(Node(2),3)
              coorx2=Coor(Node(4),1)
              coory2=Coor(Node(4),2)
              coorz2=Coor(Node(4),3)
              coorx3=Coor(Node(3),1)
              coory3=Coor(Node(3),2)
              coorz3=Coor(Node(3),3)
           CASE(2)
              coorx1=Coor(Node(1),1)
              coory1=Coor(Node(1),2)
              coorz1=Coor(Node(1),3)
              coorx2=Coor(Node(3),1)
              coory2=Coor(Node(3),2)
              coorz2=Coor(Node(3),3)
              coorx3=Coor(Node(4),1)
              coory3=Coor(Node(4),2)
              coorz3=Coor(Node(4),3)
           CASE(3)
              coorx1=Coor(Node(1),1)
              coory1=Coor(Node(1),2)
              coorz1=Coor(Node(1),3)
              coorx2=Coor(Node(4),1)
              coory2=Coor(Node(4),2)
              coorz2=Coor(Node(4),3)
              coorx3=Coor(Node(2),1)
              coory3=Coor(Node(2),2)
              coorz3=Coor(Node(2),3)
           CASE(4)
              coorx1=Coor(Node(1),1)
              coory1=Coor(Node(1),2)
              coorz1=Coor(Node(1),3)
              coorx2=Coor(Node(2),1)
              coory2=Coor(Node(2),2)
              coorz2=Coor(Node(2),3)
              coorx3=Coor(Node(3),1)
              coory3=Coor(Node(3),2)
              coorz3=Coor(Node(3),3)
           END SELECT


           !           d=SQRT(SUM(V(int_j,:)**2)) 

           vp=SQRT(Cij(J-Nflu_loc-Nflusol_loc,1,1)/rho(J))

           !----------------------------------------------------------------------------
           !*** Computation of the rhs term int_GammaExt(g * phi_j) 
           !----------------------------------------------------------------------------

           !----------------------------------------------------------------------------
           !**** Loop on the nodes of the quadrature 
           !----------------------------------------------------------------------------
           DO l=1,NGL2D

              !----------------------------------------------------------------------------
              !***** Value of exp(iw/c(..)) at the quadrature node l
              !----------------------------------------------------------------------------
              Valsourcebord(l) = EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              !----------------------------------------------------------------------------
              !***** Value of grad(exp(iw/c(..)))  at the quadrature node l
              !----------------------------------------------------------------------------
              GradValsourcebord(l,1) = -CMPLX(0,omega/Vp)*COS(theta0)*COS(theta1)&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              GradValsourcebord(l,2) = -CMPLX(0,omega/Vp)*SIN(theta0)*COS(theta1)&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              GradValsourcebord(l,3) = -CMPLX(0,omega/Vp)*SIN(theta1)&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              !----------------------------------------------------------------------------
              !***** Value of gradgrad(exp(iw/c(..)))  at the quadrature node l
              !----------------------------------------------------------------------------
              !! NOTE 1 xx, 2 yy, 3 zz, 4 yz, 5 xz, 6 xy
              GradGradValsourcebord(l,1) = (-CMPLX(0,omega/Vp)*COS(theta0)*COS(theta1))**2&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              GradGradValsourcebord(l,2) = (-CMPLX(0,omega/Vp)*SIN(theta0)*COS(theta1))**2&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              GradGradValsourcebord(l,3) = (-CMPLX(0,omega/Vp)*SIN(theta1))**2&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              GradGradValsourcebord(l,4) = (-CMPLX(0,omega/Vp)*SIN(theta0)*COS(theta1))&
                   &*(-CMPLX(0,omega/Vp)*SIN(theta1))&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              GradGradValsourcebord(l,5) = (-CMPLX(0,omega/Vp)*COS(theta0)*COS(theta1))&
                   &*(-CMPLX(0,omega/Vp)*SIN(theta1))&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))

              GradGradValsourcebord(l,6) = (-CMPLX(0,omega/Vp)*COS(theta0)*COS(theta1))&
                   &*(-CMPLX(0,omega/Vp)*SIN(theta0)*COS(theta1))&
                   &*EXP(-CMPLX(0,(omega/Vp*(&
                   &((coorx2-coorx1)*PtGl2D(l,1)+(coorx3-coorx1)*PtGl2D(l,2)+coorx1)*COS(theta0)*COS(theta1)+&
                   &((coory2-coory1)*PtGl2D(l,1)+(coory3-coory1)*PtGl2D(l,2)+coory1)*SIN(theta0)*COS(theta1)+&
                   &((coorz2-coorz1)*PtGl2D(l,1)+(coorz3-coorz1)*PtGl2D(l,2)+coorz1)*SIN(theta1) ))))
           ENDDO

           !----------------------------------------------------------------------------
           !**** Loop on the basis funtions defined on the current element
           !----------------------------------------------------------------------------
           DO K=1,nphi(degree_tri_loc(J))

              !----------------------------------------------------------------------------
              !***** Computation of each component (indexed by l) of sum_{X_q}{grad(exp(iw/c(..)))*phi_j(X_q)*w_q}
              !----------------------------------------------------------------------------
              DO l=1,dim
                 GradPhiIG(l)=SUM(value_basis%value_surface(degree_tri_loc(J))&
                      &%matphii_ptgl_surf(1)%coeff(:,K,1,int_j)*GradValsourcebord(:,l)*wgl2D(:))
              END DO

              !----------------------------------------------------------------------------
              !***** Computation of each component (indexed by l) of sum_{X_q}{gradgrad(exp(iw/c(..)))*phi_j(X_q)*w_q}
              !----------------------------------------------------------------------------
              DO l=1,6
                 GradGradPhiIG(l)=SUM(value_basis%value_surface(degree_tri_loc(J))&
                      &%matphii_ptgl_surf(1)%coeff(:,K,1,int_j)&
                      &*GradGradValsourcebord(:,l)*wgl2D(:))
              END DO
              !              id%IRHS_SPARSE(ind+K)=index_tri(triloc2glob(J)-1)+K
              !              id%IRHS_SPARSE(ind+nphi(degree_tri_loc(J))+K)&
              !                   &=index_tri(triloc2glob(J)-1)+K+nphi(degree_tri_loc(J))

              !----------------------------------------------------------------------------
              !***** Product between the matrix sum_{X_q}{sigma(u_sol)*phi_j(X_q)*w_q} and the external normal vector n
              !----------------------------------------------------------------------------

              !----------------------------------------------------------------------------
              !****** First Component
              !----------------------------------------------------------------------------
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,1,1)*GradGradPhiIG(1)*norm(int_j,1)
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,1,2)*GradGradPhiIG(2)*norm(int_j,1)
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,1,3)*GradGradPhiIG(3)*norm(int_j,1)
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,1,4)*GradGradPhiIG(4)*norm(int_j,1)*2
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,1,5)*GradGradPhiIG(5)*norm(int_j,1)*2
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,1,6)*GradGradPhiIG(6)*norm(int_j,1)*2

              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,1)*GradGradPhiIG(1)*norm(int_j,2)
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,2)*GradGradPhiIG(2)*norm(int_j,2)
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,3)*GradGradPhiIG(3)*norm(int_j,2)
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,4)*GradGradPhiIG(4)*norm(int_j,2)*2
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,5)*GradGradPhiIG(5)*norm(int_j,2)*2
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,6)*GradGradPhiIG(6)*norm(int_j,2)*2

              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,1)*GradGradPhiIG(1)*norm(int_j,3)
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,2)*GradGradPhiIG(2)*norm(int_j,3)
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,3)*GradGradPhiIG(3)*norm(int_j,3)
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,4)*GradGradPhiIG(4)*norm(int_j,3)*2
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,5)*GradGradPhiIG(5)*norm(int_j,3)*2
              id%rhs(index_tri(triloc2glob(J)-1)+K) =  id%rhs(index_tri(triloc2glob(J)-1)+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,6)*GradGradPhiIG(6)*norm(int_j,3)*2

              !----------------------------------------------------------------------------
              !****** Second Component
              !----------------------------------------------------------------------------
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,1)*GradGradPhiIG(1)*norm(int_j,1)
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,2)*GradGradPhiIG(2)*norm(int_j,1)
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,3)*GradGradPhiIG(3)*norm(int_j,1)
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,4)*GradGradPhiIG(4)*norm(int_j,1)*2
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,5)*GradGradPhiIG(5)*norm(int_j,1)*2
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,6,6)*GradGradPhiIG(6)*norm(int_j,1)*2

              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,2,1)*GradGradPhiIG(1)*norm(int_j,2)
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,2,2)*GradGradPhiIG(2)*norm(int_j,2)
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,2,3)*GradGradPhiIG(3)*norm(int_j,2)
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,2,4)*GradGradPhiIG(4)*norm(int_j,2)*2
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,2,5)*GradGradPhiIG(5)*norm(int_j,2)*2
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,2,6)*GradGradPhiIG(6)*norm(int_j,2)*2

              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,1)*GradGradPhiIG(1)*norm(int_j,3)
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,2)*GradGradPhiIG(2)*norm(int_j,3)
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,3)*GradGradPhiIG(3)*norm(int_j,3)
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,4)*GradGradPhiIG(4)*norm(int_j,3)*2
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,5)*GradGradPhiIG(5)*norm(int_j,3)*2
              id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,6)*GradGradPhiIG(6)*norm(int_j,3)*2

              !----------------------------------------------------------------------------
              !****** Third Component
              !----------------------------------------------------------------------------
              id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) =&
                   &  id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,1)*GradGradPhiIG(1)*norm(int_j,1)
              id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) =&
                   &  id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,2)*GradGradPhiIG(2)*norm(int_j,1)
              id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) =&
                   &  id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,3)*GradGradPhiIG(3)*norm(int_j,1)
              id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) =&
                   &  id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,4)*GradGradPhiIG(4)*norm(int_j,1)*2
              id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) =&
                   &  id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,5)*GradGradPhiIG(5)*norm(int_j,1)*2
              id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) =&
                   &  id%rhs(index_tri(triloc2glob(J)-1)+K+2*nphi(degree_tri_loc(J))) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,5,6)*GradGradPhiIG(6)*norm(int_j,1)*2

              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,1)*GradGradPhiIG(1)*norm(int_j,2)
              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,2)*GradGradPhiIG(2)*norm(int_j,2)
              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,3)*GradGradPhiIG(3)*norm(int_j,2)
              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,4)*GradGradPhiIG(4)*norm(int_j,2)*2
              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,5)*GradGradPhiIG(5)*norm(int_j,2)*2
              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,4,6)*GradGradPhiIG(6)*norm(int_j,2)*2

              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,3,1)*GradGradPhiIG(1)*norm(int_j,3)
              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,3,2)*GradGradPhiIG(2)*norm(int_j,3)
              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,3,3)*GradGradPhiIG(3)*norm(int_j,3)
              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,3,4)*GradGradPhiIG(4)*norm(int_j,3)*2
              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,3,5)*GradGradPhiIG(5)*norm(int_j,3)*2
              id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) = &
                   & id%rhs(index_tri(triloc2glob(J)-1)+2*nphi(degree_tri_loc(J))+K) +&
                   &Cij(J-Nflu_loc-Nflusol_loc,3,6)*GradGradPhiIG(6)*norm(int_j,3)*2

           END DO
           ind=ind+dim*nphi(degree_tri_loc(J))
        END IF
     END DO
  END DO
  ALLOCATE(idref(id%N))
  CALL MPI_ALLREDUCE(id%rhs,idref,id%N, MPI_COMPLEX_PERSO,MPI_sum&
       &,MPI_COMM_WORLD,ierr)

  id%rhs = idref

END SUBROUTINE sub_rhs3D_elastoneumann
