SUBROUTINE sub_time_loop
  USE PRECISION
  USE CONSTANT
  USE DATA
  USE mesh
  USE solution
  USE analytic 
  USE PML
  USE mpi_modif
  USE transient

  IMPLICIT NONE

  REAL(kind=dp),ALLOCATABLE :: U_aux(:,:),U_aux2(:,:)  ,P_aux(:,:),vec_source(:,:)
  REAL(kind=dp) :: alpha0,time,dt_source,energ,ratio
  INTEGER :: procineigh_loc,nt,I1,I2,I3,I4,I5,I6,I,J,irhs,ii,i_tmp,i_tmp1&
       &,i_tmp2,i_tmp3,i_tmp4,deg,N_sismo
  CHARACTER*100 :: nam,nam1,nam2,nam3

  ALLOCATE(U_aux(dim*(Nfacesperelem+1)*Nphi(MAXVAL(degree_tri_loc)),nb_rhs))
  ALLOCATE(P_aux((Nfacesperelem+1)*Nphi(MAXVAL(degree_tri_loc)),nb_rhs))
  ALLOCATE(U_aux2(dim*Nphi(MAXVAL(degree_tri_loc)),nb_rhs))
  N_sismo=1
  U_aux=0
  Nbinst=1
  IF(type_src_time.GT.0) THEN
  alpha0=-pi**2*fpeak**2
ELSEIF (type_src_time.LT.0) THEN 
   ALLOCATE(vec_source(size_source,2))
   IF(myrank.EQ.0) THEN
      OPEN(unit=52,FILE=TRIM(file_src_time))
      DO I=1,size_source
         READ(52,*) vec_source(I,:)
     END DO
     CLOSE(52)
  END IF
  CALL MPI_Bcast(vec_source, 2*size_source, MPI_REAL_PERSO, 0, MPI_COMM_WORLD,&
       & ierr)
  dt_source=(vec_source(2,1)-vec_source(1,1))
END IF
  N_simu=T_simu/dt+1
  DO Nt=1,N_simu
     energ=0.

!!! Solid Part
!!!Loop over the solid inner elements
     DO I=1,Nsolflu_inner+Nsol_inner
        I_tmp1=1  
        DO J=1,idx_vec_sol(1,I)
           I1=idx_vec_sol(2*J,I)
           I2=idx_vec_sol(2*J+1,I)
           I_tmp2=I_tmp1+I2-I1
           U_aux(I_tmp1:I_tmp2,:)=U_inter(I1:I2,:)
           I_tmp1=I_tmp2+1
        END DO
        I1=idx_vec_sol(2,I)
        I2=idx_vec_sol(3,I)
        I3=idx_mat_sol(I)
        I4=idx_mat_sol(I+1)-1

        U_new(I1:I2,1:nb_rhs)=MATMUL(RESHAPE(A_sol(I3:I4), (/I2-I1+1, I_tmp2/))&
             &,U_aux(1:I_tmp2,1:nb_rhs))-U_old(I1:I2,1:nb_rhs)
        !!Calcul energie
!!$           energ=energ+DOT_PRODUCT(MATMUL(RESHAPE(A_sol_bis(I3:I4), (/I2-I1+1&
!!$                &, I_tmp2/)),U_aux(1:I_tmp2)),U_new(I1:I2))
!!$           energ=energ+DOT_PRODUCT(MATMUL(mass_mat(deg)%coeff,U_new(I1:I2)&
!!$                &-U_inter(I1:I2))*DFVEC(I),U_new(I1:I2)&
!!$                &-U_inter(I1:I2))/dt**2

        !energ=energ+

        !           CALL DGEMV ('N', I2-I1+1, I_tmp2, 1.D0, A_sol(I3), (I2-I1+1), U_aux, 1,1.D0,&
        !                & U_new(I1), 1 )
     END DO
!!!Loop over the solid border elements



     DO I=1,Nflu_inner+Nflusol_inner
        I_tmp1=1  
        DO J=1,idx_vec_flu(1,I)
           I1=idx_vec_flu(2*J,I)
           I2=idx_vec_flu(2*J+1,I)
           I_tmp2=I_tmp1+I2-I1
           P_aux(I_tmp1:I_tmp2,1:nb_rhs)=P_inter(I1:I2,1:nb_rhs)
           I_tmp1=I_tmp2+1
        END DO
        I1=idx_vec_flu(2,I)
        I2=idx_vec_flu(3,I)
        I3=idx_mat_flu(I)
        I4=idx_mat_flu(I+1)-1

        P_new(I1:I2,1:nb_rhs)=MATMUL(RESHAPE(A_flu(I3:I4), (/I2-I1+1, I_tmp2/)),P_aux(1:I_tmp2,1:nb_rhs))-P_old(I1:I2,1:nb_rhs)
        !!Calcul energie
!!$           energ=energ+DOT_PRODUCT(MATMUL(RESHAPE(A_sol_bis(I3:I4), (/I2-I1+1&
!!$                &, I_tmp2/)),U_aux(1:I_tmp2)),U_new(I1:I2))
!!$           energ=energ+DOT_PRODUCT(MATMUL(mass_mat(deg)%coeff,U_new(I1:I2)&
!!$                &-U_inter(I1:I2))*DFVEC(I),U_new(I1:I2)&
!!$                &-U_inter(I1:I2))/dt**2

        !energ=energ+

        !           CALL DGEMV ('N', I2-I1+1, I_tmp2, 1.D0, A_sol(I3), (I2-I1+1), U_aux, 1,1.D0,&
        !                & U_new(I1), 1 )
     END DO
!!!Loop over the solid border elements


     DO Procineigh_loc=1,Nprocneigh_sol
        CALL MPI_WAIT(req_recv_sol(Procineigh_loc), status, ierr)
     END DO
     DO Procineigh_loc=1,Nprocneigh_flu
        CALL MPI_WAIT(req_recv_flu(Procineigh_loc), status, ierr)
     END DO
     DO Procineigh_loc=1,Nprocneigh_sol
        CALL MPI_WAIT(req_send_sol(Procineigh_loc), status, ierr)
     END DO
     DO Procineigh_loc=1,Nprocneigh_flu
        CALL MPI_WAIT(req_send_flu(Procineigh_loc), status, ierr)
     END DO
     i_tmp3=index_tri_loc_flu(Nflu_inner+Nflusol_inner+Nflu_border&
          &+Nflusol_border)+1
     DO I=1,Nprocneigh_flu
        tag_send_flu(I)=myrank*nproc+neighproc_flu(I)-1
        tag_recv_flu(I)=(neighproc_flu(I)-1)*nproc+myrank
        i_tmp4=index_tri_loc_flu(Nflu_inner+Nflusol_inner+Nflu_border&
             &+Nflusol_border+SUM(Nflu_ghost(1:I)))
        DO irhs=1,nB_rhs
           P_inter(i_tmp3:i_tmp4,irhs)=comm_ghost_rec_flu(I)&
                &%VALUE((i_tmp4&
                &-i_tmp3+1)*(irhs-1)+1:(i_tmp4&
                &-i_tmp3+1)*irhs)
        END DO
        i_tmp3=i_tmp4+1    
     END DO
     i_tmp3=index_tri_loc_sol(Nsolflu_inner+Nsol_inner+Nsolflu_border&
          &+Nsol_border)+1 
     DO I=1,Nprocneigh_sol
        tag_send_sol(I)=myrank*nproc+neighproc_sol(I)-1
        tag_recv_sol(I)=(neighproc_sol(I)-1)*nproc+myrank
        i_tmp4=index_tri_loc_sol(Nsolflu_inner+Nsol_inner+Nsolflu_border&
             &+Nsol_border+SUM(Nsol_ghost(1:I)))
        DO irhs=1,nB_rhs
           U_inter(i_tmp3:i_tmp4,irhs)=comm_ghost_rec_sol(I)&
                &%VALUE((i_tmp4&
                &-i_tmp3+1)*(irhs-1)+1:(i_tmp4&
                &-i_tmp3+1)*irhs)
        END DO
        i_tmp3=i_tmp4+1 
     END DO

     DO I=Nsolflu_inner+Nsol_inner+1,Nsolflu_inner+Nsol_inner+Nsolflu_border+Nsol_border
        I_tmp1=1  
        DO J=1,idx_vec_sol(1,I)
           I1=idx_vec_sol(2*J,I)
           I2=idx_vec_sol(2*J+1,I)
           I_tmp2=I_tmp1+I2-I1
           U_aux(I_tmp1:I_tmp2,1:nb_rhs)=U_inter(I1:I2,1:nb_rhs)
           I_tmp1=I_tmp2+1
        END DO
        I1=idx_vec_sol(2,I)
        I2=idx_vec_sol(3,I)
        I3=idx_mat_sol(I)
        I4=idx_mat_sol(I+1)-1
        U_new(I1:I2,1:nb_rhs)=-U_old(I1:I2,1:nb_rhs)+MATMUL(RESHAPE(A_sol(I3:I4), (/I2-I1+1, I_tmp2/)),U_aux(1:I_tmp2,1:nb_rhs))

        !!Calcul Energie
!!$           energ=energ+DOT_PRODUCT(MATMUL(RESHAPE(A_sol_bis(I3:I4), (/I2-I1+1&
!!$                &, I_tmp2/)),U_aux(1:I_tmp2)),U_new(I1:I2))
!!$           energ=energ+DOT_PRODUCT(MATMUL(mass_mat(deg)%coeff,U_new(I1:I2)&
!!$                &-U_inter(I1:I2))*DFVEC(I),U_new(I1:I2)&
!!$                &-U_inter(I1:I2))/dt**2

     END DO
     !! Calcul Energie
!!$        IF(myrank.EQ.0) THEN
!!$           energ_tot=energ
!!$           DO proci=1,nproc-1
!!$              CALL MPI_RECV(energ, 1, MPI_DOUBLE_PRECISION, procI, TAG(1), MPI_COMM_WORLD, STATUS,IERR)
!!$              energ_tot=energ_tot+energ
!!$           END DO
!!$           WRITE(34,*) Nt,energ_tot
!!$        ELSE
!!$           CALL MPI_SEND(energ, 1, MPI_DOUBLE_PRECISION, 0, TAG(1), MPI_COMM_WORLD,&
!!$                & IERR)
!!$        END IF

     DO I=Nflu_inner+Nflusol_inner+1,Nflu_inner+Nflusol_inner+Nflu_border+Nflusol_border
        I_tmp1=1  
        DO J=1,idx_vec_flu(1,I)
           I1=idx_vec_flu(2*J,I)
           I2=idx_vec_flu(2*J+1,I)
           I_tmp2=I_tmp1+I2-I1
           P_aux(I_tmp1:I_tmp2,1:nb_rhs)=P_inter(I1:I2,1:nb_rhs)
           I_tmp1=I_tmp2+1
        END DO
        I1=idx_vec_flu(2,I)
        I2=idx_vec_flu(3,I)
        I3=idx_mat_flu(I)
        I4=idx_mat_flu(I+1)-1
        P_new(I1:I2,1:nb_rhs)=-P_old(I1:I2,1:nb_rhs)+MATMUL(RESHAPE(A_flu(I3:I4), (/I2-I1+1, I_tmp2/)),P_aux(1:I_tmp2,1:nb_rhs))
        !           CALL DGEMV ('N', I2-I1+1, I_tmp2, 1.D0, A_sol(I3:I4), (I2-I1+1), U_aux(1:I_tmp2), 1,1.D0,&
        !                & U_new(I1:I2), 1 )
        !           WRITE(6,*) U_new(I1:I2)

        !!Calcul Energie
!!$           energ=energ+DOT_PRODUCT(MATMUL(RESHAPE(A_sol_bis(I3:I4), (/I2-I1+1&
!!$                &, I_tmp2/)),U_aux(1:I_tmp2)),U_new(I1:I2))
!!$           energ=energ+DOT_PRODUCT(MATMUL(mass_mat(deg)%coeff,U_new(I1:I2)&
!!$                &-U_inter(I1:I2))*DFVEC(I),U_new(I1:I2)&
!!$                &-U_inter(I1:I2))/dt**2

     END DO
     !! Calcul Energie
!!$        IF(myrank.EQ.0) THEN
!!$           energ_tot=energ
!!$           DO proci=1,nproc-1
!!$              CALL MPI_RECV(energ, 1, MPI_DOUBLE_PRECISION, procI, TAG(1), MPI_COMM_WORLD, STATUS,IERR)
!!$              energ_tot=energ_tot+energ
!!$           END DO
!!$           WRITE(34,*) Nt,energ_tot
!!$        ELSE
!!$           CALL MPI_SEND(energ, 1, MPI_DOUBLE_PRECISION, 0, TAG(1), MPI_COMM_WORLD,&
!!$                & IERR)
!!$        END IF


     !        CALL MPI_ALLREDUCE(energ,energ_tot,1, MPI_DOUBLE_PRECISION,MPI_sum,MPI_COMM_WORLD,ierr)
     !        IF(myrank.EQ.0) WRITE(34,*) Nt,energ_tot


     IF(type_src_time.GT.0) THEN
        time=(Nt-1)*dt-1.2/fpeak
        DO i=1,NB_SRCsol
           Irhs=src_loc2glob(i)
           U_new(src_idx(i,1):src_idx(i,2),irhs)=U_new(src_idx(i&
                &,1):src_idx(i,2),irhs)+(1+2.*alpha0*time**2)&
                &*EXP(alpha0*time**2)*src_val(src_idx(i,3):src_idx(i,4))

        END DO
        DO i=1,NB_SRCflu
           Irhs=src_loc2glob(i)
           P_new(src_idx(i,1):src_idx(i,2),irhs)=P_new(src_idx(i&
                &,1):src_idx(i,2),irhs)+(1+2.*alpha0*time**2)&
                &*EXP(alpha0*time**2)*src_val(src_idx(i,3):src_idx(i,4))

        END DO
  ELSEIF(type_src_time.LT.0) THEN
        time=(Nt-1)*dt
        I1=FLOOR(time/dt_source)+1
        DO i=1,NB_SRCsol           
           Irhs=src_loc2glob(i)
           IF (I1.LE.size_source-1) THEN
              U_new(src_idx(i,1):src_idx(i,2),irhs)=U_new(src_idx(i&
                   &,1):src_idx(i,2),irhs)+(vec_source(I1,2)*(-(time/dt_source&
                   &-I1))&
                   &+vec_source(I1+1,2)*(time/dt_source-I1+1))&
                   &*src_val(src_idx(i,3):src_idx(i,4))
           END IF

        END DO
     DO i=1,NB_SRCflu
        Irhs=src_loc2glob(i)
        P_new(src_idx(i,1):src_idx(i,2),irhs)=P_new(src_idx(i&
             &,1):src_idx(i,2),irhs)+(vec_source(I1,2)*(-(time/dt_source&
                   &-I1))&
                   &+vec_source(I1+1,2)*(time/dt_source-I1+1))&
                   &*src_val(src_idx(i,3):src_idx(i,4))

     END DO
     END IF



     P_old(1:index_tri_loc_flu(Nflu_inner+Nflusol_inner+Nflu_border&
          &+Nflusol_border),1:nb_rhs)=&
          &P_inter(1:index_tri_loc_flu(Nflu_inner+Nflusol_inner+Nflu_border+Nflusol_border),1:nb_rhs)
     P_inter(1:index_tri_loc_flu(Nflu_inner+Nflusol_inner+Nflu_border+Nflusol_border),1:nb_rhs)=&
          &P_new(1:index_tri_loc_flu(Nflu_inner+Nflusol_inner+Nflu_border+Nflusol_border),1:nb_rhs)


     U_old(1:index_tri_loc_sol(Nsolflu_inner+Nsol_inner+Nsolflu_border+Nsol_border),1:nb_rhs)=&
          &U_inter(1:index_tri_loc_sol(Nsolflu_inner+Nsol_inner+Nsolflu_border+Nsol_border),1:nb_rhs)
     U_inter(1:index_tri_loc_sol(Nsolflu_inner+Nsol_inner+Nsolflu_border+Nsol_border),1:nb_rhs)=&
          &U_new(1:index_tri_loc_sol(Nsolflu_inner+Nsol_inner+Nsolflu_border+Nsol_border),1:nb_rhs)
     DO I=1,Nbcla_sol
        I5=idx_mat_cla_sol(I)
        I6=idx_mat_cla_sol(I+1)-1
        II=TriCla_sol(I)
        deg=degree_tri_loc(II)
        I1=idx_vec_sol(2,II)
        I2=idx_vec_sol(3,II)
        U_old(I1:I2,1:nb_rhs)=MATMUL(RESHAPE(A_cla_sol(I5:I6), &
             &(/dim*Nphi(deg), dim*Nphi(deg)/)),U_old(I1:I2,1:nb_rhs))
     END DO

     DO I=1,Nbcla_flu
        I5=idx_mat_cla_flu(I)
        I6=idx_mat_cla_flu(I+1)-1
        II=TriCla_flu(I)
        deg=degree_tri_loc(II)
        I1=idx_vec_flu(2,II)
        I2=idx_vec_flu(3,II)
        P_old(I1:I2,1:nb_rhs)=MATMUL(RESHAPE(A_cla_flu(I5:I6), &
             &(/Nphi(deg), Nphi(deg)/)),P_old(I1:I2,1:nb_rhs))
     END DO

     !! Building, sending and receiving the communications vector    
     i_tmp3=index_tri_loc_flu(Nflu_inner+Nflusol_inner+Nflu_border&
          &+Nflusol_border)+1
     DO Procineigh_loc=1,Nprocneigh_flu
        I_tmp=1
        DO Irhs=1,nb_rhs
           DO I=1,idx_ghost_flu(Procineigh_loc)
              i_tmp1=comm_ghost_send2rcv_flu(Procineigh_loc)%VALUE(1,I)
              i_tmp2=comm_ghost_send2rcv_flu(Procineigh_loc)%VALUE(2,I)
              comm_ghost_flu(Procineigh_loc)%VALUE(I_tmp:I_tmp+i_tmp2-I_tmp1)&
                   &=P_new(i_tmp1:i_tmp2,irhs)

              I_tmp=I_tmp+i_tmp2-I_tmp1+1
           END DO
        END DO
        i_tmp4=index_tri_loc_flu(Nflu_inner+Nflusol_inner+Nflu_border&
             &+Nflusol_border+SUM(Nflu_ghost(1:Procineigh_loc)))

        CALL MPI_ISEND(comm_ghost_flu(Procineigh_loc)%VALUE,(I_tmp-1),MPI_REAL_PERSO&
             &,neighproc_flu(Procineigh_loc)-1,tag_send_flu(Procineigh_loc&
             &),MPI_COMM_WORLD,req_send_flu(Procineigh_loc),ierr)
        CALL MPI_IRECV(comm_ghost_rec_flu(Procineigh_loc)%VALUE,(i_tmp4&
             &-i_tmp3+1)*nb_rhs&
             &,MPI_REAL_PERSO,neighproc_flu(Procineigh_loc)-1&
             &,tag_recv_flu(Procineigh_loc)&
             &,MPI_COMM_WORLD,req_recv_flu(Procineigh_loc),ierr)
        i_tmp3=i_tmp4+1    
     END DO
     i_tmp3=index_tri_loc_sol(Nsolflu_inner+Nsol_inner+Nsolflu_border+Nsol_border)+1
     DO Procineigh_loc=1,Nprocneigh_sol
        I_tmp=1
        DO irhs=1,nB_rhs
           DO I=1,idx_ghost_sol(Procineigh_loc)
              i_tmp1=comm_ghost_send2rcv_sol(Procineigh_loc)%VALUE(1,I)
              i_tmp2=comm_ghost_send2rcv_sol(Procineigh_loc)%VALUE(2,I)
              comm_ghost_sol(Procineigh_loc)%VALUE(I_tmp:I_tmp+i_tmp2-I_tmp1)&
                   &=U_new(i_tmp1:i_tmp2,irhs)
              I_tmp=I_tmp+i_tmp2-I_tmp1+1
           END DO
        END DO
        i_tmp4=index_tri_loc_sol(Nsolflu_inner+Nsol_inner+Nsolflu_border&
             &+Nsol_border+SUM(Nsol_ghost(1:Procineigh_loc)))
        CALL MPI_ISEND(comm_ghost_sol(Procineigh_loc)%VALUE,(I_tmp-1),MPI_REAL_PERSO&
             &,neighproc_sol(Procineigh_loc)-1,tag_send_sol(Procineigh_loc&
             &),MPI_COMM_WORLD,req_send_sol(Procineigh_loc),ierr)
        CALL MPI_IRECV(comm_ghost_rec_sol(Procineigh_loc)%VALUE,(i_tmp4&
             &-i_tmp3+1)*nb_rhs&
             &,MPI_REAL_PERSO,neighproc_sol(Procineigh_loc)-1&
             &,tag_recv_sol(Procineigh_loc)&
             &,MPI_COMM_WORLD,req_recv_sol(Procineigh_loc),ierr)
        i_tmp3=i_tmp4+1    
     END DO
     IF(Nt*dt.GE.Nbinst*dt_snap) THEN
        IF(Nsol.GT.0) THEN
           nam1="U"
           IF(dim.EQ.2) THEN
              DO Irhs=1,nb_rhs
                 CALL sub_writeunstruct_u_binary_transient(nam1,Irhs,Nbinst)!!in lib/liboutput
              END DO
           ELSE
              DO Irhs=1,nb_rhs
                 CALL sub_writeunstruct3D_u_binary_transient(nam1,Irhs,Nbinst)!!in
                 !!lib/liboutput
              END DO
           END IF
        END IF
        IF(Nflu.GT.0) THEN
           nam1="P"
           IF(dim.EQ.2) THEN
              DO Irhs=1,nb_rhs
                CALL sub_writeunstruct_p_binary_transient(nam1,irhs,Nbinst)!!in
                 !!lib/liboutput
              END DO
           ELSE
              DO Irhs=1,nb_rhs
                 CALL sub_writeunstruct3D_p_binary_transient(nam1,irhs,Nbinst)!!in
                 !!lib/liboutput
              END DO
           END IF
        END IF
        Nbinst=Nbinst+1
     END IF
     IF((N_sismo*dt_snap.LE.Nt*dt).AND.(N_sismo*dt_snap.GE.(Nt-1)*dt)) THEN
        ratio=(N_sismo*dt_snap-(Nt-1)*dt)/dt
        CALL         sub_write_sismos_transient(ratio)
     END IF
  END DO

END SUBROUTINE sub_time_loop
