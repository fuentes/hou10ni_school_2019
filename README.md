hou10ni_school_2019
----------
- public version for the school 2019 of the hou10ni  Discontinous Galerkin simulation code for wave propagation

Dependencies
------------
- MUMPS (including Metis, Scotch, and SCALAPACK for // version)
- MAPHYS (optional but required for the school)
- BLAS (tested against Openblas)
- LAPACK

plafrim version using guix
--------------------------
guix command before cmake : 
```
 guix environment --pure --ad-hoc scotch gcc-toolchain make cmake maphys mumps-openmpi openmpi gfortran@5 coreutils grep sed findutils scalapack pt-scotch metis pastix starpu pkg-config openblas openssh -- /bin/bash --norc
```
