MODULE PML
  USE PRECISION
  TYPE data_pml
     LOGICAL :: ispmltop,ispmlbottom,ispmlleft,ispmlright,ispmlfore,ispmlback
     REAL(kind=dp) :: zb,zt,yb,yt,xl,xr 
     REAL(kind=dp) :: coeff_t,coeff_b,coeff_l,coeff_r ,coeff_fo,coeff_ba 
  END TYPE data_pml
  TYPE(data_pml), SAVE :: pmlayer
END MODULE PML
