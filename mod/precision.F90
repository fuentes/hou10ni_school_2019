!> @brief module defining kind of double and quadruple real
!> @todo change dq to qp in order to be consistent
module precision
  integer, parameter :: dq=16 !< quadruple precision
  integer, parameter :: dp=8  !< double precision
end module precision
