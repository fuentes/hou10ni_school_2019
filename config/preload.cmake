set(PREFIX /path/to/somewhere)
# MUMPS config
set(MUMPS_INCLUDE_PATH ${PREFIX}/include CACHE STRING "")
set(MUMPS_LIB_DIR ${PREFIX}/lib/ CACHE STRING "")
# METIS config
set(METIS_LIB_DIR /usr/lib/ CACHE STRING "")
# SCOTCH config
set(SCOTCH_LIB_DIR ${PREFIX}/lib CACHE STRING "")
# SCALAPACK config
set(SCALAPACK_LIB_DIR /usr/lib/ CACHE STRING "")
# blas/lapack config
set(BLAS_LIB_DIR /usr/lib/ CACHE STRING "")
set(LAPACK_LIB_DIR  /usr/lib/ CACHE STRING "")
